
<!DOCTYPE html>
<html lang="id-ID">
    <head>
        <link rel="dns-prefetch" href="https://cdn.detik.net.id"/><link rel="dns-prefetch" href="https://akcdn.detik.net.id"/><link rel="dns-prefetch" href="https://cdnv.detik.net.id"/><link rel="dns-prefetch" href="https://rec.detik.com"/><link rel="dns-prefetch" href="https://connect.detik.com"/><link rel="dns-prefetch" href="https://newrevive.detik.com"/><link rel="dns-prefetch" href="https://comment.detik.com"/><link rel="dns-prefetch" href="https://newcomment.detik.com"/><link rel="dns-prefetch" href="https://cdnstatic.detik.com"/><link rel="dns-prefetch" href="https://analytic.detik.com"/><link rel="dns-prefetch" href="https://mood.detik.com"/><link rel="dns-prefetch" href="https://connect.facebook.net"/><link rel="dns-prefetch" href="https://www.googletagmanager.com"/><link rel="dns-prefetch" href="https://platform.twitter.com"/><link rel="dns-prefetch" href="https://b.scorecardresearch.com"/><link rel="dns-prefetch" href="https://securepubads.g.doubleclick.net"/><link rel="dns-prefetch" href="https://pubads.g.doubleclick.net"/><link rel="dns-prefetch" href="https://ads.pubmatic.com"/><link rel="dns-prefetch" href="https://www.gstatic.com"/><link rel="dns-prefetch" href="https://www.google-analytics.com"/><link rel="dns-prefetch" href="https://partner.googleadservices.com"/><link rel="dns-prefetch" href="https://gum.criteo.com"/><link rel="dns-prefetch" href="https://certify-js.alexametrics.com"/><link rel="dns-prefetch" href="https://ps.eyeota.net"/><link rel="dns-prefetch" href="https://vars.hotjar.com"/><link rel="dns-prefetch" href="https://script.hotjar.com"/><link rel="dns-prefetch" href="https://t.pubmatic.com"/><link rel="dns-prefetch" href="https://cdn.ampproject.org"/>        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="initial-scale = 1.0, user-scalable = no, width=device-width, height=device-height, maximum-scale=1.0">
        <meta name="apple-mobile-web-app-capable" content="yes" />
        <link rel="amphtml" href="https://sport.detik.com/sepakbola/uefa/d-6022811/saat-thomas-tuchel-ketus-kamu-minta-saya-senyum/amp">

                <title>news</title>
                <meta name="description" content="Thomas Tuchel tidak bisa menyembunyikan kekecewaan kala Chelsea dihantam Real Madrid 1-3. Tuchel sensitif, jawab ketus pertanyaan jurnalis!" itemprop="description" /><meta name="originalTitle" content="Saat Thomas Tuchel Ketus: Kamu Minta Saya Senyum?" />
		<meta charset="utf-8">
    	<meta property="og:type" content="article" />
    	<meta property="og:site_name" content="sepakbola" />
    	<meta property="og:title" content="Saat Thomas Tuchel Ketus: Kamu Minta Saya Senyum?" />
    	<meta property="og:image" content="https://akcdn.detik.net.id/api/wm/2022/04/08/thomas-tuchel_169.jpeg?wid=54&w=650&v=1&t=jpeg" />
    	<meta property="og:description" content="Thomas Tuchel tidak bisa menyembunyikan kekecewaan kala Chelsea dihantam Real Madrid 1-3. Tuchel sensitif, jawab ketus pertanyaan jurnalis!" />
    	<meta property="og:url" content="https://sport.detik.com/sepakbola/uefa/d-6022811/saat-thomas-tuchel-ketus-kamu-minta-saya-senyum" />
    	<meta property="fb:app_id" content="187960271237149" />
    	<meta property="fb:admins" content="100000607566694" />
    	<meta property="og:image:type" content="image/jpeg" />
    	<meta property="og:image:width" content="650" />
    	<meta property="og:image:height" content="366" />
		<meta name="viewport" content="width=device-width,minimum-scale=1,initial-scale=1">
		<link href="https://plus.google.com/+detikcom" rel="publisher" />
		<meta name="copyright" content="sepakbola" itemprop="dateline" />
        <meta name="p:domain_verify" content="2057b86bf61e5a346e22a380c6fecf89" /><meta name="robots" content="noindex, nofollow" /><meta name="googlebot-news" content="noindex, nofollow" /><meta name="googlebot" content="noindex, nofollow" /><meta name="kanalid" content="2-69-70-1033"  />
        <meta name="articleid" content="6022811"  />
        <meta name="articletype" content="singlepage"  />
        <meta name="articledewasa" content="dewasatidak"  />
        <meta name="articlehoax" content="default"  />
        <meta name="createdate" content="2022/04/08 13:17:56"  />
        <meta name="publishdate" content="2022/04/08 14:00:00"  />
        <meta name="contenttype" content="singlepagenews"  />
        <meta name="platform" content="desktop"  />
        <meta name="hl_nhl_wp" content="nonheadline-0"  />
        <meta name="hl_nhl_kanal" content="nonheadline-0-2-69"  />
        <meta name="videopresent" content="No"  />
        
        
        
        <meta name="idfokus" content=""  />
        <meta name="author" content="Afif Farhan"  />
        <meta content="Thomas Tuchel tidak bisa menyembunyikan kekecewaan kala Chelsea dihantam Real Madrid 1-3. Tuchel sensitif, jawab ketus pertanyaan jurnalis!" itemprop="headline" />
        <meta name="keywords" content="thomas tuchel,chelsea,real madrid,perempatfinal liga champions" itemprop="keywords" />
        <meta name="thumbnailUrl" content="https://akcdn.detik.net.id/community/media/visual/2022/04/08/thomas-tuchel.jpeg?w=650" itemprop="thumbnailUrl" />
        <meta property="article:author" content="https://www.facebook.com/detiksportcom" itemprop="author" />
        <meta property="article:publisher" content="https://www.facebook.com/detiksportcom" />
        <meta name="pubdate" content="2022-04-08T14-00-00Z" itemprop="datePublished" />
        <meta content="2022-04-08T13-17-56Z" itemprop="dateCreated" />
        <meta content="https://sport.detik.com/sepakbola/uefa/d-6022811/saat-thomas-tuchel-ketus-kamu-minta-saya-senyum" itemprop="url" />
        
        <meta name="twitter:card" content="summary_large_image" />
        <meta name="twitter:site" content="@detiksport" />
        <meta name="twitter:site:id" content="@detiksport" />
        <meta name="twitter:creator" content="@detiksport" />
        <meta name="twitter:description" content="Thomas Tuchel tidak bisa menyembunyikan kekecewaan kala Chelsea dihantam Real Madrid 1-3. Tuchel sensitif, jawab ketus pertanyaan jurnalis!" />
        <meta name="twitter:image" content="https://akcdn.detik.net.id/api/wm/2022/04/08/thomas-tuchel_169.jpeg?wid=60&w=650&v=1&t=jpeg" />

        
        <link rel="canonical" href="https://sport.detik.com/sepakbola/uefa/d-6022811/saat-thomas-tuchel-ketus-kamu-minta-saya-senyum" />
        <meta name="dtk:acctype" content="acc-detiksepakbola"  />
        <meta name="dtk:kanalid" content="1033"  />
        <meta name="dtk:articleid" content="6022811"  />
        <meta name="dtk:articletype" content="singlepage"  />
        <meta name="dtk:articledewasa" content="dewasatidak"  />
        <meta name="dtk:articlehoax" content="default"  />
        <meta name="dtk:createddate" content="2022/04/08 13:17:56"  />
        <meta name="dtk:publishdate" content="2022/04/08 14:00:00"  />
        <meta name="dtk:createddateunix" content="1649398676000"  />
        <meta name="dtk:publishdateunix" content="1649401200000"  />
        <meta name="dtk:contenttype" content="singlepagenews"  />
        <meta name="dtk:platform" content="desktop"  />
        <meta name="dtk:videopresent" content="No"  />
        
        <meta name="dtk:idfokus" content=""  />
        <meta name="dtk:author" content="Afif Farhan"  />
        <meta name="dtk:keywords" content="thomas tuchel,chelsea,real madrid,perempatfinal liga champions" itemprop="keywords" />
        <meta name="dtk:thumbnailUrl" content="https://akcdn.detik.net.id/community/media/visual/2022/04/08/thomas-tuchel.jpeg?w=650" itemprop="thumbnailUrl" />
        <meta name="dtk:status" content="1"/>
<script type="application/ld+json">
{
    "@context": "https://schema.org",
    "@type": "BreadcrumbList",
    "itemListElement": [
    {
        "@type": "ListItem",
        "position": 1,
        "name": "detikSport",
        "item": "https://sport.detik.com/"
    },
    {
        "@type": "ListItem",
        "position": 2,
        "name": "Sepakbola",
        "item": "https://sport.detik.com/sepakbola/"
    },
    {
        "@type": "ListItem",
        "position": 3,
        "name": "UEFA",
        "item": "https://sport.detik.com/sepakbola/uefa"
    }
]}
</script>

    	<script type="application/ld+json">
    	{
    		"@context": "https://schema.org",
    		"@type": "WebPage",
    		"headline": "Saat Thomas Tuchel Ketus: Kamu Minta Saya Senyum?",
    		"url": "https://sport.detik.com/sepakbola/uefa/d-6022811/saat-thomas-tuchel-ketus-kamu-minta-saya-senyum",
    		"datePublished": "2022-04-08T14:00:00+07:00",
    		"image": "https://akcdn.detik.net.id/community/media/visual/2022/04/08/thomas-tuchel.jpeg",
    		"thumbnailUrl": "https://akcdn.detik.net.id/community/media/visual/2022/04/08/thomas-tuchel.jpeg?w=200"
    	}
    	</script>
        <script type="application/ld+json">
		{
			"@context": "https://schema.org",
			"@type": "NewsArticle",
			"mainEntityOfPage": {
				"@type": "WebPage",
				"@id": "https://sport.detik.com/sepakbola/uefa/d-6022811/saat-thomas-tuchel-ketus-kamu-minta-saya-senyum"
			},
			"headline": "Saat Thomas Tuchel Ketus: Kamu Minta Saya Senyum?",
			"image": {
				"@type": "ImageObject",
			    "url": "https://akcdn.detik.net.id/community/media/visual/2022/04/08/thomas-tuchel.jpeg"
            },
			"datePublished": "2022-04-08T14:00:00+07:00",
			"dateModified": "2022-04-08T14:00:00+07:00",
			"author": {
				"@type": "Person",
				"name": "Afif Farhan"
			},
			"publisher": {
				"@type": "Organization",
				"name": "detikcom",
				"logo": {
					"@type": "ImageObject",
					"url": "https://cdn.detik.net.id/detik2/images/logo.jpg"
				}
			},
			"description": "Thomas Tuchel tidak bisa menyembunyikan kekecewaan kala Chelsea dihantam Real Madrid 1-3. Tuchel sensitif, jawab ketus pertanyaan jurnalis!"
		}
    	</script>
    	<script>var wid_method = "GET";</script>
        <script type="text/javascript" src="https://cdn.detik.net.id/assets/js/jquery-min-3.2.1.js?v=2022040815"></script>
        <script type="text/javascript" src="https://cdn.detik.net.id/assets/js/gtmdl.js?v=2022040815"></script>

        <!--s:dtkprv-->
<!-- S: (script) Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-NG6BTJ');</script>
<!-- E: (script) Google Tag Manager -->
<!--e:dtkprv-->
        <link href="https://cdn.detik.net.id/sepakbola2/images/favicon-sepakbola.ico?v=2022040815" rel="shortcut icon" type="image/x-icon" />

        <!--s:dtkprv-->
<!--e:dtkprv-->
        <script type="text/javascript" src="https://cdn.detik.net.id/assets/js/plugins.js?v=2022040815"></script>
        <script type="text/javascript" src="https://cdn.detik.net.id/libs/livecounter/detikLiveUserCounterResponse.js?v=2022040815"></script>
                    <link href="https://cdn.detik.net.id/sepakbola2/css/detail.css?v=2022040815" rel="stylesheet">
                <link href="https://cdn.detik.net.id/assets/css/placeholder.css?v=2022040815" rel="stylesheet">

        <!-- S:taghead --> <link rel="stylesheet" type="text/css" href="https://cdn.detik.net.id/commerce/desktop/css/detik.ads-new.css">
<script src="https://cdn.detik.net.id/commerce/commerce/dtk_commerce.js"></script>



<!-- START PUBMATIC -->
<script type="text/javascript">
                var PWT={}; //Initialize Namespace
                var googletag = googletag || {};
                googletag.cmd = googletag.cmd || [];
                var gptRan = false;
                PWT.jsLoaded = function(){ //PubMatic pwt.js on load callback is used to load GPT
                    loadGPT();
                };
                var loadGPT = function() {
                    // Check the gptRan flag
                    if (!gptRan) {
                        gptRan = true;
                        var gads = document.createElement('script');
                        var useSSL = 'https:' == document.location.protocol;
                        gads.src = (useSSL ? 'https:' : 'http:') + '//www.googletagservices.com/tag/js/gpt.js';
                        var node = document.getElementsByTagName('script')[0];
                        node.parentNode.insertBefore(gads, node);
                    }
                };
                // Failsafe to call gpt
                setTimeout(loadGPT, 500);
            </script>
            
<script type="text/javascript">
(function() {
	var purl = window.location.href;
	var url = '//ads.pubmatic.com/AdServer/js/pwt/156981/927';
	var profileVersionId = '';
	if(purl.indexOf('pwtv=')>0){
		var regexp = /pwtv=(.*?)(&|$)/g;
		var matches = regexp.exec(purl);
		if(matches.length >= 2 && matches[1].length > 0){
			profileVersionId = '/'+matches[1];
		}
	}
	var wtads = document.createElement('script');
	wtads.async = true;
	wtads.type = 'text/javascript';
	wtads.src = url+profileVersionId+'/pwt.js';
	var node = document.getElementsByTagName('script')[0];
	node.parentNode.insertBefore(wtads, node);
})();
</script>

<!-- END PUBMATIC -->



<!-- START TAG DFP -->
<script async src="https://securepubads.g.doubleclick.net/tag/js/gpt.js"></script>
<script>
var gptadslots = [];
window.googletag = window.googletag || {cmd: []};

dfp_keywords = getKeywords();
googletag.cmd.push(function() {


  gpt_leaderboard =  googletag.defineSlot('/4905536/detik_desktop/sepakbola/leaderboard', [728, 90], 'div-gpt-ad-1534407669768-0').addService(googletag.pubads());

  gpt_mediumrectangle2 = "";
googletag.defineSlot('/4905536/detik_desktop/sepakbola/medium_rectangle2', [300,250], 'div-gpt-ad-1534408024708-0').addService(googletag.pubads());

gpt_bottomframe = "";

gpt_bottomframe2 = googletag.defineSlot('/4905536/detik_desktop/sepakbola/bottomframe', [[1, 1], [728, 90], [970,50]], 'div-gpt-ad-1605763052301-0').addService(googletag.pubads());

gpt_billboardtop = googletag.defineSlot('/4905536/detik_desktop/sepakbola/billboard', [970, 250], 'div-gpt-ad-1585806489557-0').addService(googletag.pubads());
gpt_billboard = "";

  gpt_mediumrectangle1 = googletag.defineSlot('/4905536/detik_desktop/sepakbola/medium_rectangle1', [[300, 250], [300, 500], [300, 600]], 'div-gpt-ad-1534407897257-0').addService(googletag.pubads());

  googletag.defineSlot('/4905536/detik_desktop/sepakbola/medium_rectangle3', [300, 250], 'div-gpt-ad-1556897709376-0').addService(googletag.pubads());

  googletag.defineSlot('/4905536/detik_desktop/sepakbola/newsfeed1', ['fluid'], 'div-gpt-ad-1534408411376-0').setTargeting('pos', ['newsfeed1']).addService(googletag.pubads());

googletag.defineSlot('/4905536/detik_desktop/sepakbola/newsfeed2', ['fluid'], 'div-gpt-ad-1534408611127-0').setTargeting('pos', ['newsfeed2']).addService(googletag.pubads());

googletag.defineSlot('/4905536/detik_desktop/sepakbola/skyscrapper', [[160,600],[120,600]], 'div-gpt-ad-1923948-1').addService(googletag.pubads());

    //googletag.defineSlot('/4905536/detik_desktop/sepakbola/center2', [1, 1], 'div-gpt-ad-1621321281933-0').addService(googletag.pubads());

    googletag.defineSlot('/4905536/detik_desktop/sepakbola/newstag', [1, 1], 'div-gpt-ad-1621407652950-0').addService(googletag.pubads());

    googletag.defineOutOfPageSlot('/4905536/detik_mobile/sepakbola/inbetween', 'div-gpt-ad-1630595453311-0').addService(googletag.pubads());

googletag.defineSlot('/4905536/detik_desktop/sepakbola/balloon_ads', [1, 1], 'div-gpt-ad-1587568268835-0').addService(googletag.pubads());

//googletag.defineSlot('/4905536/detik_desktop/sepakbola/hiddenquiz', [1, 1], 'div-gpt-ad-1605763109113-0').addService(googletag.pubads());

googletag.defineSlot('/4905536/detik_desktop/sepakbola/parallax', [[1, 1], [300, 250]], 'div-gpt-ad-1572942873602-0').addService(googletag.pubads());

googletag.defineSlot('/4905536/detik_desktop/sepakbola/parallax_detail', [[1, 1], [300, 250]], 'div-gpt-ad-1572510301589-0').addService(googletag.pubads());

gpt_balloon = "";
//googletag.defineSlot('/4905536/detik_desktop/sepakbola/balloon_ads', [[300, 600], [400, 250], [300, 500]], 'div-gpt-ad-1611048723560-0').addService(googletag.pubads());

gpt_topframe = "";

/* S- Custom DFP For Callback Function */

googletag.pubads().addEventListener('slotRenderEnded', function(event) {
try {
if(event.slot == gpt_billboardtop && !event.isEmpty){
$("#megabillboard-desktop").height('250px');
}
   if(event.slot == gpt_billboard && !event.isEmpty){
        $(document).ready(function(){
         $('#billboard_banner').show();
           Billboard_Sticky();
        })
  }
   if (event.slot == gpt_mediumrectangle1 && !event.isEmpty ){
  $(window).scroll(function(){stickyMR1();})
}
if (event.slot == gpt_mediumrectangle2 && !event.isEmpty ){
  detectHeightMR2();
  $(window).scroll(function(){stickyMR2();})
}
   if (event.slot == gpt_balloon && !event.isEmpty) {
 var containsAdBalloon = !event.isEmpty,
 widthBalloon = event.size[0],
 heightBalloon = event.size[1];
  $('.balloon').show();
  $('.balloon_close').show();
 if (heightBalloon == 500 && widthBalloon == 300){
    $('.balloon').css({'height':'500px','width':'300px'});
    $('.balloon_close').css({'top':'0px'});
   }
}
if (event.slot == gpt_bottomframe2 && !event.isEmpty){
    var containsAdBF = !event.isEmpty,
        widthBF = event.size[0],
        heightBF = event.size[1];
    if (heightBF == 1 && widthBF == 1){
        $('.bottom_banner_bar').show();
        $('.bottom_banner_bar').css("min-height","1px");
        $('.close_tbot').hide();
    }
    else if (heightBF == 90 && widthBF == 728){
        $('.bottom_banner_bar').css("min-height","90px");
        $('.close_tbot').css("margin-top","-105px");
        $('.bottom_banner_bar').show();
        $('#adv-callback').show();
        $('.close_tbot').show();
    }
    else if (heightBF == 50 && widthBF == 970){
        $('.bottom_banner_bar').css("min-height","50px");
        $('.close_tbot').css("margin-top","-65px");
        $('.bottom_banner_bar').show();
        $('.close_tbot').show();
    }
    else{
        $('.close_tbot').hide();
        $('.bottom_banner_bar').hide();
    }
}
    if (event.slot == gpt_topframe && !event.isEmpty ){
          $('.top_banner_bar').height('50px');
          $('.close_tb').show();
    }
   if (event.slot == gpt_bottomframe && !event.isEmpty){
          $('.close_tbot').show();
          $('.bottom_banner_bar').show();
   }
   if(event.slot == gpt_leaderboard && event.isEmpty){
         removeElement('header__leaderboard');
   }
}
catch (e) {
   console.log(e);
}
});
 /*E- Custom DFP */

  googletag.pubads().enableSingleRequest();
  googletag.pubads().setTargeting('site', ['detikcom']);
  googletag.pubads().setTargeting('section', ['news']);
  googletag.pubads().setTargeting('medium', ['desktop']);
  googletag.pubads().setTargeting('keyvalue', dfp_keywords);
    googletag.pubads().setTargeting('Keyword_tag', dfp_keywords);
  googletag.pubads().setTargeting('militaryconflict', dfp_keywords);
  googletag.pubads().setTargeting('ilegal_drugs', dfp_keywords);
  googletag.pubads().setTargeting('adult', dfp_keywords);
  googletag.pubads().setTargeting('death_injury', dfp_keywords);
  googletag.pubads().setTargeting('hate_speech', dfp_keywords);
  googletag.pubads().setTargeting('spam_harmfulsite', dfp_keywords);
  googletag.pubads().setTargeting('tobacco', dfp_keywords);
  googletag.pubads().setTargeting('disaster', dfp_keywords);
  googletag.pubads().setTargeting('politic', dfp_keywords);
  googletag.pubads().setTargeting('obscenity', dfp_keywords);
  googletag.pubads().setTargeting('terorism', dfp_keywords);
  googletag.pubads().setTargeting('arms', dfp_keywords);
  googletag.pubads().setTargeting('crime', dfp_keywords);
  googletag.pubads().setTargeting('online_piracy', dfp_keywords);
  googletag.pubads().collapseEmptyDivs(true);
  googletag.enableServices();

});
function removeElement(classname) {
 var elem = document.getElementsByClassName(classname)[0];
 return elem.parentNode.removeChild(elem);
}
</script>
<!-- End Tag DFP -->




<!-- Start eyeota -->
<script>
var EO_PID='6bioi0v';
var EO_SID='detik';
</script>
<script src="https://cdn.detik.net.id/libs/js-itportal/portal.dc.js?v=1.0"></script>
<script src="https://cdnstatic.detik.com/live/js/eyeotadtk.js"></script>
<!-- End eyeota -->

<!-- detiknews -->



<!-- spotx -->

<script>
spotxDataLayer = [{
  source: "280136",
  sync_limit: 7
}];
</script>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','spotxDataLayer','GTM-NH3RQL3');</script>
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NH3RQL3&source=280136&sync_limit=7"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>

<!-- spotx -->

<!-- Pubstack tag -->
<script async src="https://boot.pbstck.com/v1/tag/443d0e84-cea4-46f6-aeba-a4634faa3cfd"></script>


<!-- CMP (Consent Management Platform) GDPR -->

<script>(function(){/*

Copyright The Closure Library Authors.
SPDX-License-Identifier: Apache-2.0
*/
'use strict';var g=function(a){var b=0;return function(){return b<a.length?{done:!1,value:a[b++]}:{done:!0}}},l=this||self,m=/^[\w+/_-]+[=]{0,2}$/,p=null,q=function(){},r=function(a){var b=typeof a;if("object"==b)if(a){if(a instanceof Array)return"array";if(a instanceof Object)return b;var c=Object.prototype.toString.call(a);if("[object Window]"==c)return"object";if("[object Array]"==c||"number"==typeof a.length&&"undefined"!=typeof a.splice&&"undefined"!=typeof a.propertyIsEnumerable&&!a.propertyIsEnumerable("splice"))return"array";
if("[object Function]"==c||"undefined"!=typeof a.call&&"undefined"!=typeof a.propertyIsEnumerable&&!a.propertyIsEnumerable("call"))return"function"}else return"null";else if("function"==b&&"undefined"==typeof a.call)return"object";return b},u=function(a,b){function c(){}c.prototype=b.prototype;a.prototype=new c;a.prototype.constructor=a};var v=function(a,b){Object.defineProperty(l,a,{configurable:!1,get:function(){return b},set:q})};var y=function(a,b){this.b=a===w&&b||"";this.a=x},x={},w={};var aa=function(a,b){a.src=b instanceof y&&b.constructor===y&&b.a===x?b.b:"type_error:TrustedResourceUrl";if(null===p)b:{b=l.document;if((b=b.querySelector&&b.querySelector("script[nonce]"))&&(b=b.nonce||b.getAttribute("nonce"))&&m.test(b)){p=b;break b}p=""}b=p;b&&a.setAttribute("nonce",b)};var z=function(){return Math.floor(2147483648*Math.random()).toString(36)+Math.abs(Math.floor(2147483648*Math.random())^+new Date).toString(36)};var A=function(a,b){b=String(b);"application/xhtml+xml"===a.contentType&&(b=b.toLowerCase());return a.createElement(b)},B=function(a){this.a=a||l.document||document};B.prototype.appendChild=function(a,b){a.appendChild(b)};var C=function(a,b,c,d,e,f){try{var k=a.a,h=A(a.a,"SCRIPT");h.async=!0;aa(h,b);k.head.appendChild(h);h.addEventListener("load",function(){e();d&&k.head.removeChild(h)});h.addEventListener("error",function(){0<c?C(a,b,c-1,d,e,f):(d&&k.head.removeChild(h),f())})}catch(n){f()}};var ba=l.atob("aHR0cHM6Ly93d3cuZ3N0YXRpYy5jb20vaW1hZ2VzL2ljb25zL21hdGVyaWFsL3N5c3RlbS8xeC93YXJuaW5nX2FtYmVyXzI0ZHAucG5n"),ca=l.atob("WW91IGFyZSBzZWVpbmcgdGhpcyBtZXNzYWdlIGJlY2F1c2UgYWQgb3Igc2NyaXB0IGJsb2NraW5nIHNvZnR3YXJlIGlzIGludGVyZmVyaW5nIHdpdGggdGhpcyBwYWdlLg=="),da=l.atob("RGlzYWJsZSBhbnkgYWQgb3Igc2NyaXB0IGJsb2NraW5nIHNvZnR3YXJlLCB0aGVuIHJlbG9hZCB0aGlzIHBhZ2Uu"),ea=function(a,b,c){this.b=a;this.f=new B(this.b);this.a=null;this.c=[];this.g=!1;this.i=b;this.h=c},F=function(a){if(a.b.body&&!a.g){var b=
function(){D(a);l.setTimeout(function(){return E(a,3)},50)};C(a.f,a.i,2,!0,function(){l[a.h]||b()},b);a.g=!0}},D=function(a){for(var b=G(1,5),c=0;c<b;c++){var d=H(a);a.b.body.appendChild(d);a.c.push(d)}b=H(a);b.style.bottom="0";b.style.left="0";b.style.position="fixed";b.style.width=G(100,110).toString()+"%";b.style.zIndex=G(2147483544,2147483644).toString();b.style["background-color"]=I(249,259,242,252,219,229);b.style["box-shadow"]="0 0 12px #888";b.style.color=I(0,10,0,10,0,10);b.style.display=
"flex";b.style["justify-content"]="center";b.style["font-family"]="Roboto, Arial";c=H(a);c.style.width=G(80,85).toString()+"%";c.style.maxWidth=G(750,775).toString()+"px";c.style.margin="24px";c.style.display="flex";c.style["align-items"]="flex-start";c.style["justify-content"]="center";d=A(a.f.a,"IMG");d.className=z();d.src=ba;d.style.height="24px";d.style.width="24px";d.style["padding-right"]="16px";var e=H(a),f=H(a);f.style["font-weight"]="bold";f.textContent=ca;var k=H(a);k.textContent=da;J(a,
e,f);J(a,e,k);J(a,c,d);J(a,c,e);J(a,b,c);a.a=b;a.b.body.appendChild(a.a);b=G(1,5);for(c=0;c<b;c++)d=H(a),a.b.body.appendChild(d),a.c.push(d)},J=function(a,b,c){for(var d=G(1,5),e=0;e<d;e++){var f=H(a);b.appendChild(f)}b.appendChild(c);c=G(1,5);for(d=0;d<c;d++)e=H(a),b.appendChild(e)},G=function(a,b){return Math.floor(a+Math.random()*(b-a))},I=function(a,b,c,d,e,f){return"rgb("+G(Math.max(a,0),Math.min(b,255)).toString()+","+G(Math.max(c,0),Math.min(d,255)).toString()+","+G(Math.max(e,0),Math.min(f,
255)).toString()+")"},H=function(a){a=A(a.f.a,"DIV");a.className=z();return a},E=function(a,b){0>=b||null!=a.a&&0!=a.a.offsetHeight&&0!=a.a.offsetWidth||(fa(a),D(a),l.setTimeout(function(){return E(a,b-1)},50))},fa=function(a){var b=a.c;var c="undefined"!=typeof Symbol&&Symbol.iterator&&b[Symbol.iterator];b=c?c.call(b):{next:g(b)};for(c=b.next();!c.done;c=b.next())(c=c.value)&&c.parentNode&&c.parentNode.removeChild(c);a.c=[];(b=a.a)&&b.parentNode&&b.parentNode.removeChild(b);a.a=null};var ia=function(a,b,c,d,e){var f=ha(c),k=function(n){n.appendChild(f);l.setTimeout(function(){f?(0!==f.offsetHeight&&0!==f.offsetWidth?b():a(),f.parentNode&&f.parentNode.removeChild(f)):a()},d)},h=function(n){document.body?k(document.body):0<n?l.setTimeout(function(){h(n-1)},e):b()};h(3)},ha=function(a){var b=document.createElement("div");b.className=a;b.style.width="1px";b.style.height="1px";b.style.position="absolute";b.style.left="-10000px";b.style.top="-10000px";b.style.zIndex="-10000";return b};var K={},L=null;var M=function(){},N="function"==typeof Uint8Array,O=function(a,b){a.b=null;b||(b=[]);a.j=void 0;a.f=-1;a.a=b;a:{if(b=a.a.length){--b;var c=a.a[b];if(!(null===c||"object"!=typeof c||Array.isArray(c)||N&&c instanceof Uint8Array)){a.g=b-a.f;a.c=c;break a}}a.g=Number.MAX_VALUE}a.i={}},P=[],Q=function(a,b){if(b<a.g){b+=a.f;var c=a.a[b];return c===P?a.a[b]=[]:c}if(a.c)return c=a.c[b],c===P?a.c[b]=[]:c},R=function(a,b,c){a.b||(a.b={});if(!a.b[c]){var d=Q(a,c);d&&(a.b[c]=new b(d))}return a.b[c]};
M.prototype.h=N?function(){var a=Uint8Array.prototype.toJSON;Uint8Array.prototype.toJSON=function(){var b;void 0===b&&(b=0);if(!L){L={};for(var c="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789".split(""),d=["+/=","+/","-_=","-_.","-_"],e=0;5>e;e++){var f=c.concat(d[e].split(""));K[e]=f;for(var k=0;k<f.length;k++){var h=f[k];void 0===L[h]&&(L[h]=k)}}}b=K[b];c=[];for(d=0;d<this.length;d+=3){var n=this[d],t=(e=d+1<this.length)?this[d+1]:0;h=(f=d+2<this.length)?this[d+2]:0;k=n>>2;n=(n&
3)<<4|t>>4;t=(t&15)<<2|h>>6;h&=63;f||(h=64,e||(t=64));c.push(b[k],b[n],b[t]||"",b[h]||"")}return c.join("")};try{return JSON.stringify(this.a&&this.a,S)}finally{Uint8Array.prototype.toJSON=a}}:function(){return JSON.stringify(this.a&&this.a,S)};var S=function(a,b){return"number"!==typeof b||!isNaN(b)&&Infinity!==b&&-Infinity!==b?b:String(b)};M.prototype.toString=function(){return this.a.toString()};var T=function(a){O(this,a)};u(T,M);var U=function(a){O(this,a)};u(U,M);var ja=function(a,b){this.c=new B(a);var c=R(b,T,5);c=new y(w,Q(c,4)||"");this.b=new ea(a,c,Q(b,4));this.a=b},ka=function(a,b,c,d){b=new T(b?JSON.parse(b):null);b=new y(w,Q(b,4)||"");C(a.c,b,3,!1,c,function(){ia(function(){F(a.b);d(!1)},function(){d(!0)},Q(a.a,2),Q(a.a,3),Q(a.a,1))})};var la=function(a,b){V(a,"internal_api_load_with_sb",function(c,d,e){ka(b,c,d,e)});V(a,"internal_api_sb",function(){F(b.b)})},V=function(a,b,c){a=l.btoa(a+b);v(a,c)},W=function(a,b,c){for(var d=[],e=2;e<arguments.length;++e)d[e-2]=arguments[e];e=l.btoa(a+b);e=l[e];if("function"==r(e))e.apply(null,d);else throw Error("API not exported.");};var X=function(a){O(this,a)};u(X,M);var Y=function(a){this.h=window;this.a=a;this.b=Q(this.a,1);this.f=R(this.a,T,2);this.g=R(this.a,U,3);this.c=!1};Y.prototype.start=function(){ma();var a=new ja(this.h.document,this.g);la(this.b,a);na(this)};
var ma=function(){var a=function(){if(!l.frames.googlefcPresent)if(document.body){var b=document.createElement("iframe");b.style.display="none";b.style.width="0px";b.style.height="0px";b.style.border="none";b.style.zIndex="-1000";b.style.left="-1000px";b.style.top="-1000px";b.name="googlefcPresent";document.body.appendChild(b)}else l.setTimeout(a,5)};a()},na=function(a){var b=Date.now();W(a.b,"internal_api_load_with_sb",a.f.h(),function(){var c;var d=a.b,e=l[l.btoa(d+"loader_js")];if(e){e=l.atob(e);
e=parseInt(e,10);d=l.btoa(d+"loader_js").split(".");var f=l;d[0]in f||"undefined"==typeof f.execScript||f.execScript("var "+d[0]);for(;d.length&&(c=d.shift());)d.length?f[c]&&f[c]!==Object.prototype[c]?f=f[c]:f=f[c]={}:f[c]=null;c=Math.abs(b-e);c=1728E5>c?0:c}else c=-1;0!=c&&(W(a.b,"internal_api_sb"),Z(a,Q(a.a,6)))},function(c){Z(a,c?Q(a.a,4):Q(a.a,5))})},Z=function(a,b){a.c||(a.c=!0,a=new l.XMLHttpRequest,a.open("GET",b,!0),a.send())};(function(a,b){l[a]=function(c){for(var d=[],e=0;e<arguments.length;++e)d[e-0]=arguments[e];l[a]=q;b.apply(null,d)}})("__d3lUW8vwsKlB__",function(a){"function"==typeof window.atob&&(a=window.atob(a),a=new X(a?JSON.parse(a):null),(new Y(a)).start())});}).call(this);

window.__d3lUW8vwsKlB__("WyJkOWU3MGQ2ODJmMjY0YjI3IixbbnVsbCxudWxsLG51bGwsImh0dHBzOi8vZnVuZGluZ2Nob2ljZXNtZXNzYWdlcy5nb29nbGUuY29tL2YvQUdTS1d4VmFjMURaRkpNcWZhdmZKSnJfcjdMc3VlcWxNaDZWZ01QWll1VzUwbzBRVXcwR1VueW01VXliTWJBRmFMZ0JGaW5WMzZHX0tmbDNCU2xac3NHVUt5MFx1MDAzZCJdCixbMjAsImRpdi1ncHQtYWQiLDEwMCwiWkRsbE56QmtOamd5WmpJMk5HSXlOd1x1MDAzZFx1MDAzZCIsW251bGwsbnVsbCxudWxsLCJodHRwczovL3d3dy5nc3RhdGljLmNvbS8wZW1uL2YvcC9kOWU3MGQ2ODJmMjY0YjI3LmpzP3VzcXBcdTAwM2RDQUUiXQpdCiwiaHR0cHM6Ly9mdW5kaW5nY2hvaWNlc21lc3NhZ2VzLmdvb2dsZS5jb20vbC9BR1NLV3hVeWExclRVTXJ5eFFqUXA2czI4dDRCVlU4TTV1ZjJzNFl3WXpuYklURHlIdkNmS3FkaXRQZ3RxMDdvRnhEZVhwN1JxOC1WcVE3TzBpYk1SYVZPP2FiXHUwMDNkMSIsImh0dHBzOi8vZnVuZGluZ2Nob2ljZXNtZXNzYWdlcy5nb29nbGUuY29tL2wvQUdTS1d4WFRvQkdWMk9DLTI1MW9yU1VzZ2VOU0JIV1lFNzREZDVpajAyYWlxUjBfNUg1WDlCbHd5VDR2V3hsT3VFVnB5NlFkalN2azFrRkd0c2c0ZmlubD9hYlx1MDAzZDJcdTAwMjZzYmZcdTAwM2QxIiwiaHR0cHM6Ly9mdW5kaW5nY2hvaWNlc21lc3NhZ2VzLmdvb2dsZS5jb20vbC9BR1NLV3hWNldFam8yNHdjYUxPMzBGRXluem93SF81MURkeU0wdFhXVFhDckZac0VZTXlnemk0eHZ0RTV5dkRXbDQ0REx0SHJDZ0hXOEx5NDdibnltSGpIP3NiZlx1MDAzZDIiXQo=");</script>

<!-- Unblock tag -->
<script defer src="https://cdn.unblockia.com/h.js"></script><!-- E:taghead -->
        <script type="text/javascript">
    var baseurl    = "https://sport.detik.com/sepakbola",
        asset_url  = "https://cdn.detik.net.id/sepakbola2",
        site_id    = "27",
        channel_id = "70",
        responsive = false</script>
<!--profile_ad:detik_sepakbola-->
<!--layout_type:desktop-->
            </head>
    <body>
        <!--s:dtkprv-->
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NG6BTJ"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<!--e:dtkprv-->


<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

<link rel="stylesheet" href="https://cdn.detik.net.id/assets/css/framebar/framebar.css?v=2022040815" type="text/css">
<link rel="stylesheet" href="https://cdn.detik.net.id/assets/css/framebar/autocomplete.css?v=2022040815" type="text/css">


<script type="text/javascript">
    var dc_params={client_id:27,ui:'popup',site_id:27}
</script>

<div class="framebar-desktop">
    <div class="dtkframebar">
        <div class="dtkframebar__bg">
            <div class="dtkframebar__wrapper">
                <div class="dtkframebar__menu dtkframebar__clearfix pull-left">
                    <a href="#" class="dtkframebar__menu__icon pull-left">
                        <span></span>
                        <span></span>
                        <span></span>
                    </a>
                    <span class="dtkframebar__menu__text">MENU</span>
                </div>
                <div class="dtkframebar__menu__kanal">
                    <ul class="newlist">
                                                <li class="sub_list">
                            <a href="https://www.detik.com/?tagfrom=framebar">
                                <span class="dtkframebar__menu__kanal__icon gtm_link_detikcom">
                                <img src="https://cdn.detik.net.id/assets/images/framebar/favicon-detikcom.png?v=2022040815" alt=""></span> detikcom
                            </a>
                            <ul class="newlist-double">
                                                                                                <li><a href="https://www.detik.com/terpopuler" target="_blank">
                                    <span class="dtkframebar__menu__kanal__icon">
                                        <img src="https://cdn.detik.net.id/framebarasset/ic_Most-3x.png" alt="">
                                    </span>
                                    Terpopuler                                                                    </a></li>
                                                                <li><a href="https://news.detik.com/kolom/kirim" target="_blank">
                                    <span class="dtkframebar__menu__kanal__icon">
                                        <img src="https://cdn.detik.net.id/framebarasset/ic_kirim_tulisan-3x.png" alt="">
                                    </span>
                                    Kirim Tulisan                                                                    </a></li>
                                                                <li><a href="https://20.detik.com/live/" target="_blank">
                                    <span class="dtkframebar__menu__kanal__icon">
                                        <img src="https://cdn.detik.net.id/framebarasset/ic_livetv.png" alt="">
                                    </span>
                                    Live TV                                                                            <span class="kanal__new">NEW</span>
                                                                    </a></li>
                                                                <li><a href="https://www.detik.com/ramadan/" target="_blank">
                                    <span class="dtkframebar__menu__kanal__icon">
                                        <img src="https://akcdn.detik.net.id/community/media/visual/2022/04/01/icon-detikramadan-2022.png" alt="">
                                    </span>
                                    Ramadan                                                                            <span class="kanal__new">NEW</span>
                                                                    </a></li>
                                                                                            </ul>
                        </li>
                    </ul>

                    <div class="kat_divide">
                        Kategori Berita
                    </div>
                    <ul class="newlist-double">
                                                                        <li>
                            <a href="https://news.detik.com" target="_blank">
                                <span class="dtkframebar__menu__kanal__icon gtm_link_detiknews">
                                <img src="https://cdn.detik.net.id/framebarasset/ic_detik-3x.png" alt=""></span> News                                                            </a>
                        </li>
                                                <li>
                            <a href="https://finance.detik.com" target="_blank">
                                <span class="dtkframebar__menu__kanal__icon gtm_link_detiknews">
                                <img src="https://cdn.detik.net.id/framebarasset/ic_finance-3x.png" alt=""></span> Finance                                                            </a>
                        </li>
                                                <li>
                            <a href="https://inet.detik.com" target="_blank">
                                <span class="dtkframebar__menu__kanal__icon gtm_link_detiknews">
                                <img src="https://cdn.detik.net.id/framebarasset/ic_inet-3x.png" alt=""></span> Teknologi                                                            </a>
                        </li>
                                                <li>
                            <a href="https://hot.detik.com" target="_blank">
                                <span class="dtkframebar__menu__kanal__icon gtm_link_detiknews">
                                <img src="https://cdn.detik.net.id/framebarasset/ic_hot-3x.png" alt=""></span> Entertaiment                                                            </a>
                        </li>
                                                <li>
                            <a href="https://sport.detik.com" target="_blank">
                                <span class="dtkframebar__menu__kanal__icon gtm_link_detiknews">
                                <img src="https://cdn.detik.net.id/framebarasset/ic_sport-3x.png" alt=""></span> Sport                                                            </a>
                        </li>
                                                <li>
                            <a href="https://sport.detik.com/sepakbola" target="_blank">
                                <span class="dtkframebar__menu__kanal__icon gtm_link_detiknews">
                                <img src="https://cdn.detik.net.id/framebarasset/ic_sepakbola-3x.png" alt=""></span> Sepakbola                                                            </a>
                        </li>
                                                <li>
                            <a href="https://oto.detik.com" target="_blank">
                                <span class="dtkframebar__menu__kanal__icon gtm_link_detiknews">
                                <img src="https://cdn.detik.net.id/framebarasset/ic_oto-3x.png" alt=""></span> Otomotif                                                            </a>
                        </li>
                                                <li>
                            <a href="https://travel.detik.com" target="_blank">
                                <span class="dtkframebar__menu__kanal__icon gtm_link_detiknews">
                                <img src="https://cdn.detik.net.id/framebarasset/ic_travel-3x.png" alt=""></span> Travel                                                            </a>
                        </li>
                                                <li>
                            <a href="https://food.detik.com" target="_blank">
                                <span class="dtkframebar__menu__kanal__icon gtm_link_detiknews">
                                <img src="https://cdn.detik.net.id/framebarasset/ic_food-3x.png" alt=""></span> Food                                                            </a>
                        </li>
                                                <li>
                            <a href="https://health.detik.com" target="_blank">
                                <span class="dtkframebar__menu__kanal__icon gtm_link_detiknews">
                                <img src="https://cdn.detik.net.id/framebarasset/ic_health-3x.png" alt=""></span> Health                                                            </a>
                        </li>
                                                <li>
                            <a href="https://wolipop.detik.com" target="_blank">
                                <span class="dtkframebar__menu__kanal__icon gtm_link_detiknews">
                                <img src="https://cdn.detik.net.id/framebarasset/ic_wolipop-3x.png" alt=""></span> Wolipop                                                            </a>
                        </li>
                                                <li>
                            <a href="https://news.detik.com/x/" target="_blank">
                                <span class="dtkframebar__menu__kanal__icon gtm_link_detiknews">
                                <img src="https://cdn.detik.net.id/framebarasset/ic_X-3x.png" alt=""></span> DetikX                                                            </a>
                        </li>
                                                <li>
                            <a href="https://20.detik.com" target="_blank">
                                <span class="dtkframebar__menu__kanal__icon gtm_link_detiknews">
                                <img src="https://cdn.detik.net.id/framebarasset/ic_20D-3x.png" alt=""></span> 20Detik                                                            </a>
                        </li>
                                                <li>
                            <a href="https://foto.detik.com" target="_blank">
                                <span class="dtkframebar__menu__kanal__icon gtm_link_detiknews">
                                <img src="https://cdn.detik.net.id/framebarasset/ic_foto.png" alt=""></span> Foto                                                            </a>
                        </li>
                                                <li>
                            <a href="https://www.detik.com/edu/" target="_blank">
                                <span class="dtkframebar__menu__kanal__icon gtm_link_detiknews">
                                <img src="https://cdn.detik.net.id/framebarasset/ic_edu-3x.png" alt=""></span> Edukasi                                                                    <span class="kanal__new">NEW</span>
                                                            </a>
                        </li>
                                                                    </ul>

                    <div class="kat_divide">
                        Daerah
                    </div>
                    <ul class="newlist-double">
                                                                        <li>
                            <a dtr-evt="header" dtr-sec="" dtr-act="menu" onclick="_pt(this)" dtr-idx="Jateng" dtr-ttl="menu hamburger" href="https://www.detik.com/jateng" target="_blank">
                                <span class="dtkframebar__menu__kanal__icon gtm_link_detiknews">
                                <img src="https://cdn.detik.net.id/framebarasset/ic_jateng-3x.png" alt=""></span> Jateng                                                            </a>
                        </li>
                                                <li>
                            <a dtr-evt="header" dtr-sec="" dtr-act="menu" onclick="_pt(this)" dtr-idx="Jatim" dtr-ttl="menu hamburger" href="https://www.detik.com/jatim" target="_blank">
                                <span class="dtkframebar__menu__kanal__icon gtm_link_detiknews">
                                <img src="https://cdn.detik.net.id/framebarasset/ic_jatim-3x.png" alt=""></span> Jatim                                                            </a>
                        </li>
                                                <li>
                            <a dtr-evt="header" dtr-sec="" dtr-act="menu" onclick="_pt(this)" dtr-idx="Jabar" dtr-ttl="menu hamburger" href="https://www.detik.com/jabar" target="_blank">
                                <span class="dtkframebar__menu__kanal__icon gtm_link_detiknews">
                                <img src="https://cdn.detik.net.id/framebarasset/ic_jabar-3x.png" alt=""></span> Jabar                                                            </a>
                        </li>
                                                <li>
                            <a dtr-evt="header" dtr-sec="" dtr-act="menu" onclick="_pt(this)" dtr-idx="Sulsel" dtr-ttl="menu hamburger" href="https://www.detik.com/sulsel" target="_blank">
                                <span class="dtkframebar__menu__kanal__icon gtm_link_detiknews">
                                <img src="https://cdn.detik.net.id/framebarasset/ic_sulsel-2x.png" alt=""></span> Sulsel                                                            </a>
                        </li>
                                                <li>
                            <a dtr-evt="header" dtr-sec="" dtr-act="menu" onclick="_pt(this)" dtr-idx="Sumut" dtr-ttl="menu hamburger" href="https://www.detik.com/sumut" target="_blank">
                                <span class="dtkframebar__menu__kanal__icon gtm_link_detiknews">
                                <img src="https://cdn.detik.net.id/framebarasset/ic_sumut-3x.png" alt=""></span> Sumut                                                            </a>
                        </li>
                                                <li>
                            <a dtr-evt="header" dtr-sec="" dtr-act="menu" onclick="_pt(this)" dtr-idx="Bali" dtr-ttl="menu hamburger" href="https://www.detik.com/bali" target="_blank">
                                <span class="dtkframebar__menu__kanal__icon gtm_link_detiknews">
                                <img src="https://cdn.detik.net.id/framebarasset/ic_detikbali-3x.png" alt=""></span> Bali                                                            </a>
                        </li>
                                                                    </ul>

                    <div class="kat_divide">
                        Layanan
                    </div>
                    <ul class="newlist-double">
                                                                        <li>
                            <a href="https://pasangmata.detik.com" target="_blank">
                                <span class="dtkframebar__menu__kanal__icon gtm_link_pasangmata">
                                <img src="https://cdn.detik.net.id/framebarasset/ic_pasang_mata-3x.png" alt=""></span> Pasang Mata                                                            </a>
                        </li>
                                                <li>
                            <a href="https://adsmart.detik.com" target="_blank">
                                <span class="dtkframebar__menu__kanal__icon gtm_link_pasangmata">
                                <img src="https://cdn.detik.net.id/framebarasset/ic_adsmart-3x.png" alt=""></span> Ads Smart                                                            </a>
                        </li>
                                                <li>
                            <a href="https://forum.detik.com" target="_blank">
                                <span class="dtkframebar__menu__kanal__icon gtm_link_pasangmata">
                                <img src="https://cdn.detik.net.id/framebarasset/ic_forum-3x.png" alt=""></span> Forum                                                            </a>
                        </li>
                                                <li>
                            <a href="https://event.detik.com" target="_blank">
                                <span class="dtkframebar__menu__kanal__icon gtm_link_pasangmata">
                                <img src="https://cdn.detik.net.id/framebarasset/ic_event-3x.png" alt=""></span> detikEvent                                                            </a>
                        </li>
                                                <li>
                            <a href="https://www.transsnowworld.com" target="_blank">
                                <span class="dtkframebar__menu__kanal__icon gtm_link_pasangmata">
                                <img src="https://cdn.detik.net.id/framebarasset/trans_snow.png" alt=""></span> Trans Snow World                                                            </a>
                        </li>
                                                <li>
                            <a href="https://www.transstudiocibubur.com" target="_blank">
                                <span class="dtkframebar__menu__kanal__icon gtm_link_pasangmata">
                                <img src="https://cdn.detik.net.id/framebarasset/trans_cibubur.png" alt=""></span> Trans Studio Cibubur                                                            </a>
                        </li>
                                                <li>
                            <a href="https://www.transstudiobali.com" target="_blank">
                                <span class="dtkframebar__menu__kanal__icon gtm_link_pasangmata">
                                <img src="https://cdn.detik.net.id/framebarasset/ic_bali.png" alt=""></span> Trans Studio Bali                                                            </a>
                        </li>
                                                <li>
                            <a href="https://www.berbuatbaik.id/?utm_source=detikcom&utm_medium=framebar&utm_campaign=layanan" target="_blank">
                                <span class="dtkframebar__menu__kanal__icon gtm_link_pasangmata">
                                <img src="https://akcdn.detik.net.id/community/media/visual/2021/11/24/logo-berbuatbaikid.png" alt=""></span> berbuatbaik.id                                                            </a>
                        </li>
                                                                    </ul>

                    <div class="kat_divide">
                        Detik Network
                    </div>
                    <ul class="newlist-double">
                                                                        <li>
                            <a href="https://www.cnnindonesia.com" target="_blank">
                                <span class="dtkframebar__menu__kanal__icon">
                                <img src="https://cdn.detik.net.id/framebarasset/ic_cnn-3x.png" alt=""></span> CNN Indonesia                                                            </a>
                        </li>
                                                <li>
                            <a href="https://www.cnbcindonesia.com" target="_blank">
                                <span class="dtkframebar__menu__kanal__icon">
                                <img src="https://cdn.detik.net.id/framebarasset/ic_CNBC-3x.png" alt=""></span> CNBC Indonesia                                                            </a>
                        </li>
                                                <li>
                            <a href="https://www.haibunda.com" target="_blank">
                                <span class="dtkframebar__menu__kanal__icon">
                                <img src="https://cdn.detik.net.id/framebarasset/ic_hai_bunda-3x.png" alt=""></span> Hai Bunda                                                            </a>
                        </li>
                                                <li>
                            <a href="https://www.insertlive.com" target="_blank">
                                <span class="dtkframebar__menu__kanal__icon">
                                <img src="https://cdn.detik.net.id/framebarasset/ic_insert-3x.png" alt=""></span> InsertLive                                                            </a>
                        </li>
                                                <li>
                            <a href="https://www.beautynesia.id" target="_blank">
                                <span class="dtkframebar__menu__kanal__icon">
                                <img src="https://cdn.detik.net.id/framebarasset/ic_beautynesia-1.png" alt=""></span> Beautynesia                                                            </a>
                        </li>
                                                <li>
                            <a href="https://www.femaledaily.com" target="_blank">
                                <span class="dtkframebar__menu__kanal__icon">
                                <img src="https://cdn.detik.net.id/framebarasset/ic_female_dialy.png" alt=""></span> Female Daily                                                            </a>
                        </li>
                                                <li>
                            <a href="https://cxomedia.id" target="_blank">
                                <span class="dtkframebar__menu__kanal__icon">
                                <img src="https://cdn.cxomedia.id/assets/images/favicon/favicon-bg-Black-33.png?v=0.1.7" alt=""></span> CXO Media                                                            </a>
                        </li>
                                                                    </ul>
                </div>

                <div class="dtkframebar__search pull-left gtm_framebardc_search">
                    <div class="dtkframebar__search__input gtm_framebardc_btn_search">
                        <form id="search_navbar" action="https://www.detik.com/search/searchall?">
                            <input name="query" type="text" class="text" placeholder="Cari Berita" autocomplete="off">
                            <input name="siteid" type="hidden" value="27">
                            <button class="dtkframebar__icons dtkframebar__icons-search"></button>
                        </form>
                    </div>
                </div>

                <div class="dtkframebar__user pull-right">
                    <div class="dtkframebar__user__login" style="border-left:none;">
                        <div id="status_user_nf">
                            <div class="dtkframebar__user__login__in">
                                <div style="display:inline; padding-right:200px;">
                                    <div class="ph-item">
                                        <div class="ph-col-12">
                                            <div class="ph-row">
                                                <div class="ph-col-12 big" style="margin-top: 3px;margin-bottom: 0px;height: 25px;border-radius: 12px;"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="dtkframebar__bg__inner"></div>
            </div>
        </div>
    </div>
</div>


<script>
  var navBlockFlag = true;
  var loginStatusFlag = true;

    $(document).on('DOMSubtreeModified', '.dtkframebar', function() {

      if (navBlockFlag) {
        var $menuState = $('.dtkframebar__menu__icon, .dtkframebar__menu__kanal');      // attribute to check menu state
        var $sidemenuToggle = $('.dtkframebar__menu__icon, .dtkframebar__menu__text');  // button that trigger sidemenu open

        $sidemenuToggle.click(function() {
          if ($menuState.hasClass('show')) {
            try {
                hj('trigger', 'sidemenu_close');
            } catch (e) {
                console.log(e);
            }
            // console.log('hotjar: close sidemenu');
          } else {
              try {
            hj('trigger', 'sidemenu_open');
        } catch (e) {
            console.log(e);
        }
            // console.log('hotjar: open sidemenu');
          }
        });

        navBlockFlag = false;
      }

      $(document).on('DOMSubtreeModified', '.dtkframebar__user', function() {
        if (loginStatusFlag) {
          setTimeout(function() {
            $('.msk, .to_login').click(function() {
              // console.log('hotjar: popup login');
              try {
              hj('trigger', 'popup_detikid');
          } catch (e) {
              console.log(e);
          }
            });
          }, 100);

          loginStatusFlag = false;
        }
      });
    });


</script>


<!-- S:skinner --><div class="bn_skin bn_skin1">
    <div class="skin3 slot_skinnerkiri">
        <ins data-labelname="skinnerkiri" data-revive-zoneid="569" data-revive-id="0cceecb9cae9f51a31123c541910d59b"></ins>
    </div>
</div>
<div class="bn_skin bn_skin2">
    <div class="skin3 slot_skinnerkanan">
       <ins data-labelname="skinnerkanan" data-revive-zoneid="570" data-revive-id="0cceecb9cae9f51a31123c541910d59b"></ins>
    </div>
</div><!-- E:skinner -->
<!-- S:topframe --><center>
<div class="parallax1 parallaxB" id="megabillboard-desktop" style="width: 100%;height: 0px;position: relative;margin-bottom:10px !important;margin-top:0 !important;max-width:970px;left:0;right:0;margin:auto;">
<div class="parallax_abs">
<div class="parallax_fix" style="max-width: 970px;">
<div class="parallax_ads" style="top:46px">
<!-- /4905536/detik_desktop/sepakbola/billboard -->
<div id='div-gpt-ad-1585806489557-0'>
<script>
googletag.cmd.push(function() { googletag.display('div-gpt-ad-1585806489557-0'); });
</script>
</div>
</div>
</div>
</div>
</div>
</center>
<script>

$("body").css({'overflow' : 'hidden','height' : '100%'});
$("html").css({'overflow' : 'hidden','height' : '100%'});
$(document).ready(function(){
setTimeout(function(){ $("body").css({'overflow' : '','height' : 'auto'});$("html").css({'overflow' : '','height' : 'auto'});}, 3000);
})

</script><!-- E:topframe -->
<!-- S:prestitial --><!-- E:prestitial -->
<header class="header">
    <div class="container">
        <div class="grid-row no-gutter flex-between">
            <div class="column-auto header__logo">
                <a href='https://sport.detik.com/sepakbola/' dtr-evt="header" dtr-sec="logo detikSepakbola" dtr-act="logo detikSepakbola" onclick="_pt(this)"><img src='https://akcdn.detik.net.id/community/media/visual/2019/06/27/344c08b8-596e-4493-8554-4e78915b5f51.png?d=1' title='Sepakbola' alt='sepakbola' class='logodetik' /></a>            </div>
            <!-- S:leaderboard --><style>
.container { width: 1080px; }
@media (min-width: 1024px) and (max-width: 1280px) { .container { width: 1020px; }
}
</style>
<div class="column-auto header__leaderboard">
<!-- /4905536/detik_desktop/sepakbola/leaderboard -->
<div id='div-gpt-ad-1534407669768-0'>
<script>
googletag.cmd.push(function() { googletag.display('div-gpt-ad-1534407669768-0'); });
</script>
</div>
</div><!-- E:leaderboard -->        </div>

        <div class="navbar navbar--bg">
            <nav class="navbar-first">
                <a dtr-evt="header" dtr-sec="" dtr-act="first navbar" onclick="_pt(this)" dtr-ttl="logo detikcom"                    href="https://www.detik.com/"
                    class="navbar-logo">
                    <img src="https://cdn.detik.net.id/assets/images/favicon/favicon-detik-big.png?v=2022040815" alt="icon detikcom">
                </a>
                <ul class="nav">
                                                                                                            <li class="nav__item ">
                            <a dtr-evt="header" dtr-sec="" dtr-act="first navbar" onclick="_pt(this)" dtr-idx="1" dtr-ttl="Home"                                href="https://sport.detik.com/sepakbola/">
                                Home                            </a>
                        </li>
                                                                    <li class="nav__item ">
                            <a dtr-evt="header" dtr-sec="" dtr-act="first navbar" onclick="_pt(this)" dtr-idx="2" dtr-ttl="Liga Inggris"                                href="https://sport.detik.com/sepakbola/liga-inggris">
                                Liga Inggris                            </a>
                        </li>
                                                                    <li class="nav__item ">
                            <a dtr-evt="header" dtr-sec="" dtr-act="first navbar" onclick="_pt(this)" dtr-idx="3" dtr-ttl="Liga Italia"                                href="https://sport.detik.com/sepakbola/liga-italia">
                                Liga Italia                            </a>
                        </li>
                                                                    <li class="nav__item ">
                            <a dtr-evt="header" dtr-sec="" dtr-act="first navbar" onclick="_pt(this)" dtr-idx="4" dtr-ttl="Liga Spanyol"                                href="https://sport.detik.com/sepakbola/liga-spanyol">
                                Liga Spanyol                            </a>
                        </li>
                                                                    <li class="nav__item ">
                            <a dtr-evt="header" dtr-sec="" dtr-act="first navbar" onclick="_pt(this)" dtr-idx="5" dtr-ttl="Liga Jerman"                                href="https://sport.detik.com/sepakbola/liga-jerman">
                                Liga Jerman                            </a>
                        </li>
                                                                    <li class="nav__item ">
                            <a dtr-evt="header" dtr-sec="" dtr-act="first navbar" onclick="_pt(this)" dtr-idx="6" dtr-ttl="Liga Indonesia"                                href="https://sport.detik.com/sepakbola/liga-indonesia">
                                Liga Indonesia                            </a>
                        </li>
                                                                    <li class="nav__item ">
                            <a dtr-evt="header" dtr-sec="" dtr-act="first navbar" onclick="_pt(this)" dtr-idx="7" dtr-ttl="UEFA"                                href="https://sport.detik.com/sepakbola/uefa">
                                UEFA                            </a>
                        </li>
                                                                    <li class="nav__item ">
                            <a dtr-evt="header" dtr-sec="" dtr-act="first navbar" onclick="_pt(this)" dtr-idx="8" dtr-ttl="Dunia"                                href="https://sport.detik.com/sepakbola/bola-dunia">
                                Dunia                            </a>
                        </li>
                                                                    <li class="nav__item ">
                            <a dtr-evt="header" dtr-sec="" dtr-act="first navbar" onclick="_pt(this)" dtr-idx="9" dtr-ttl="Indeks"                                href="https://sport.detik.com/sepakbola/indeks">
                                Indeks                            </a>
                        </li>
                                                                    <li class="nav__item ">
                            <a dtr-evt="header" dtr-sec="" dtr-act="first navbar" onclick="_pt(this)" dtr-idx="10" dtr-ttl="detikSport"                                href="https://sport.detik.com">
                                detikSport                            </a>
                        </li>
                                    </ul>
            </nav>
            <nav class="navbar-second">
                <ul class="nav">
                                            <li class="nav__item">
                            <a dtr-evt="header" dtr-sec="" dtr-act="second navbar" onclick="_pt(this)" dtr-idx="1" dtr-ttl="Terpopuler"                                href="https://www.detik.com/terpopuler/sepakbola">
                                Terpopuler                            </a>
                        </li>
                                            <li class="nav__item">
                            <a dtr-evt="header" dtr-sec="" dtr-act="second navbar" onclick="_pt(this)" dtr-idx="2" dtr-ttl="Jadwal"                                href="https://sport.detik.com/sepakbola/jadwal">
                                Jadwal                            </a>
                        </li>
                                            <li class="nav__item">
                            <a dtr-evt="header" dtr-sec="" dtr-act="second navbar" onclick="_pt(this)" dtr-idx="3" dtr-ttl="Klasemen Liga"                                href="https://sport.detik.com/sepakbola/klasemen">
                                Klasemen Liga                            </a>
                        </li>
                                            <li class="nav__item">
                            <a dtr-evt="header" dtr-sec="" dtr-act="second navbar" onclick="_pt(this)" dtr-idx="4" dtr-ttl="Foto"                                href="https://sport.detik.com/sepakbola/foto">
                                Foto                            </a>
                        </li>
                                            <li class="nav__item">
                            <a dtr-evt="header" dtr-sec="" dtr-act="second navbar" onclick="_pt(this)" dtr-idx="5" dtr-ttl="Video"                                href="https://sport.detik.com/sepakbola/video">
                                Video                            </a>
                        </li>
                                            <li class="nav__item">
                            <a dtr-evt="header" dtr-sec="" dtr-act="second navbar" onclick="_pt(this)" dtr-idx="6" dtr-ttl="Infografis"                                href="https://sport.detik.com/sepakbola/infografis">
                                Infografis                            </a>
                        </li>
                                            <li class="nav__item">
                            <a dtr-evt="header" dtr-sec="" dtr-act="second navbar" onclick="_pt(this)" dtr-idx="7" dtr-ttl="Man Of The Match"                                href="https://sport.detik.com/sepakbola/man-of-the-match">
                                Man Of The Match                            </a>
                        </li>
                                            <li class="nav__item">
                            <a dtr-evt="header" dtr-sec="" dtr-act="second navbar" onclick="_pt(this)" dtr-idx="8" dtr-ttl="Gila Bola"                                href="https://sport.detik.com/sepakbola/gila-bola">
                                Gila Bola                            </a>
                        </li>
                    
                    <!-- S:navbar --><li class="nav__item nav__item--new">
<ins data-labelname="navbar" data-revive-zoneid="1077" data-revive-id="0cceecb9cae9f51a31123c541910d59b"></ins>
</li><!-- E:navbar -->		            <!-- S:navbar2 --><ins data-labelname="navbar2" data-revive-zoneid="1448" data-revive-id="0cceecb9cae9f51a31123c541910d59b"></ins><!-- E:navbar2 -->					<!-- S:navbar3 --><!-- E:navbar3 -->                </ul>
            </nav>
        </div>
        <div class="navbar navbar--team">
    <nav class="navbar-third">
        <ul class="nav" id="liga_team">
            <li class="nav__item">
                <form>
                    <select id="wid_select_team" class="form-element">
                                                   <option value="liga-inggris" >Liga Inggris </option>
                                                   <option value="liga-italia" >Liga Italia</option>
                                                   <option value="liga-spanyol" >Liga Spanyol</option>
                                                   <option value="liga-jerman" >Liga Jerman</option>
                                                   <option value="liga-indonesia" >Liga Indonesia</option>
                                            </select>
                </form>
            </li>
             <li class="nav__item liga_team_logo">
 <a dtr-evt="header" dtr-sec="" dtr-act="tim navbar" onclick="_pt(this)" dtr-idx="1" dtr-ttl="Man City" href="https://sport.detik.com/sepakbola/profil-tim/liga-inggris/manchester-city-fc"
 class="navbar-logo">
 <img src="https://cdn.detik.net.id/sportasset/soccerteamsset/logo_with_contestant_id/a3nyxabgsqlnqfkeg41m6tnpp.png" alt="Man City">
 <span class="tooltip">
 Man City </span>
 </a>
 </li>
 <li class="nav__item liga_team_logo">
 <a dtr-evt="header" dtr-sec="" dtr-act="tim navbar" onclick="_pt(this)" dtr-idx="2" dtr-ttl="Liverpool" href="https://sport.detik.com/sepakbola/profil-tim/liga-inggris/liverpool-fc"
 class="navbar-logo">
 <img src="https://cdn.detik.net.id/sportasset/soccerteamsset/logo_with_contestant_id/c8h9bw1l82s06h77xxrelzhur.png" alt="Liverpool">
 <span class="tooltip">
 Liverpool </span>
 </a>
 </li>
 <li class="nav__item liga_team_logo">
 <a dtr-evt="header" dtr-sec="" dtr-act="tim navbar" onclick="_pt(this)" dtr-idx="3" dtr-ttl="Chelsea" href="https://sport.detik.com/sepakbola/profil-tim/liga-inggris/chelsea-fc"
 class="navbar-logo">
 <img src="https://cdn.detik.net.id/sportasset/soccerteamsset/logo_with_contestant_id/9q0arba2kbnywth8bkxlhgmdr.png" alt="Chelsea">
 <span class="tooltip">
 Chelsea </span>
 </a>
 </li>
 <li class="nav__item liga_team_logo">
 <a dtr-evt="header" dtr-sec="" dtr-act="tim navbar" onclick="_pt(this)" dtr-idx="4" dtr-ttl="Tottenham" href="https://sport.detik.com/sepakbola/profil-tim/liga-inggris/tottenham-hotspur-fc"
 class="navbar-logo">
 <img src="https://cdn.detik.net.id/sportasset/soccerteamsset/logo_with_contestant_id/22doj4sgsocqpxw45h607udje.png" alt="Tottenham">
 <span class="tooltip">
 Tottenham </span>
 </a>
 </li>
 <li class="nav__item liga_team_logo">
 <a dtr-evt="header" dtr-sec="" dtr-act="tim navbar" onclick="_pt(this)" dtr-idx="5" dtr-ttl="Arsenal" href="https://sport.detik.com/sepakbola/profil-tim/liga-inggris/arsenal-fc"
 class="navbar-logo">
 <img src="https://cdn.detik.net.id/sportasset/soccerteamsset/logo_with_contestant_id/4dsgumo7d4zupm2ugsvm4zm4d.png" alt="Arsenal">
 <span class="tooltip">
 Arsenal </span>
 </a>
 </li>
 <li class="nav__item liga_team_logo">
 <a dtr-evt="header" dtr-sec="" dtr-act="tim navbar" onclick="_pt(this)" dtr-idx="6" dtr-ttl="West Ham" href="https://sport.detik.com/sepakbola/profil-tim/liga-inggris/west-ham-united-fc"
 class="navbar-logo">
 <img src="https://cdn.detik.net.id/sportasset/soccerteamsset/logo_with_contestant_id/4txjdaqveermfryvbfrr4taf7.png" alt="West Ham">
 <span class="tooltip">
 West Ham </span>
 </a>
 </li>
 <li class="nav__item liga_team_logo">
 <a dtr-evt="header" dtr-sec="" dtr-act="tim navbar" onclick="_pt(this)" dtr-idx="7" dtr-ttl="Man Utd" href="https://sport.detik.com/sepakbola/profil-tim/liga-inggris/manchester-united-fc"
 class="navbar-logo">
 <img src="https://cdn.detik.net.id/sportasset/soccerteamsset/logo_with_contestant_id/6eqit8ye8aomdsrrq0hk3v7gh.png" alt="Man Utd">
 <span class="tooltip">
 Man Utd </span>
 </a>
 </li>
 <li class="nav__item liga_team_logo">
 <a dtr-evt="header" dtr-sec="" dtr-act="tim navbar" onclick="_pt(this)" dtr-idx="8" dtr-ttl="Wolves" href="https://sport.detik.com/sepakbola/profil-tim/liga-inggris/wolverhampton-wanderers-fc"
 class="navbar-logo">
 <img src="https://cdn.detik.net.id/sportasset/soccerteamsset/logo_with_contestant_id/b9si1jn1lfxfund69e9ogcu2n.png" alt="Wolves">
 <span class="tooltip">
 Wolves </span>
 </a>
 </li>
 <li class="nav__item liga_team_logo">
 <a dtr-evt="header" dtr-sec="" dtr-act="tim navbar" onclick="_pt(this)" dtr-idx="9" dtr-ttl="Crystal Palace" href="https://sport.detik.com/sepakbola/profil-tim/liga-inggris/crystal-palace-fc"
 class="navbar-logo">
 <img src="https://cdn.detik.net.id/sportasset/soccerteamsset/logo_with_contestant_id/1c8m2ko0wxq1asfkuykurdr0y.png" alt="Crystal Palace">
 <span class="tooltip">
 Crystal Palace </span>
 </a>
 </li>
 <li class="nav__item liga_team_logo">
 <a dtr-evt="header" dtr-sec="" dtr-act="tim navbar" onclick="_pt(this)" dtr-idx="10" dtr-ttl="Leicester" href="https://sport.detik.com/sepakbola/profil-tim/liga-inggris/leicester-city-fc"
 class="navbar-logo">
 <img src="https://cdn.detik.net.id/sportasset/soccerteamsset/logo_with_contestant_id/avxknfz4f6ob0rv9dbnxdzde0.png" alt="Leicester">
 <span class="tooltip">
 Leicester </span>
 </a>
 </li>
 <li class="nav__item liga_team_logo">
 <a dtr-evt="header" dtr-sec="" dtr-act="tim navbar" onclick="_pt(this)" dtr-idx="11" dtr-ttl="Aston Villa" href="https://sport.detik.com/sepakbola/profil-tim/liga-inggris/aston-villa-fc"
 class="navbar-logo">
 <img src="https://cdn.detik.net.id/sportasset/soccerteamsset/logo_with_contestant_id/b496gs285it6bheuikox6z9mj.png" alt="Aston Villa">
 <span class="tooltip">
 Aston Villa </span>
 </a>
 </li>
 <li class="nav__item liga_team_logo">
 <a dtr-evt="header" dtr-sec="" dtr-act="tim navbar" onclick="_pt(this)" dtr-idx="12" dtr-ttl="Southampton" href="https://sport.detik.com/sepakbola/profil-tim/liga-inggris/southampton-fc"
 class="navbar-logo">
 <img src="https://cdn.detik.net.id/sportasset/soccerteamsset/logo_with_contestant_id/d5ydtvt96bv7fq04yqm2w2632.png" alt="Southampton">
 <span class="tooltip">
 Southampton </span>
 </a>
 </li>
 <li class="nav__item liga_team_logo">
 <a dtr-evt="header" dtr-sec="" dtr-act="tim navbar" onclick="_pt(this)" dtr-idx="13" dtr-ttl="Brighton" href="https://sport.detik.com/sepakbola/profil-tim/liga-inggris/brighton-&-hove-albion-fc"
 class="navbar-logo">
 <img src="https://cdn.detik.net.id/sportasset/soccerteamsset/logo_with_contestant_id/e5p0ehyguld7egzhiedpdnc3w.png" alt="Brighton">
 <span class="tooltip">
 Brighton </span>
 </a>
 </li>
 <li class="nav__item liga_team_logo">
 <a dtr-evt="header" dtr-sec="" dtr-act="tim navbar" onclick="_pt(this)" dtr-idx="14" dtr-ttl="Brentford" href="https://sport.detik.com/sepakbola/profil-tim/liga-inggris/brentford-fc"
 class="navbar-logo">
 <img src="https://cdn.detik.net.id/sportasset/soccerteamsset/logo_with_contestant_id/7yx5dqhhphyvfisohikodajhv.png" alt="Brentford">
 <span class="tooltip">
 Brentford </span>
 </a>
 </li>
 <li class="nav__item liga_team_logo">
 <a dtr-evt="header" dtr-sec="" dtr-act="tim navbar" onclick="_pt(this)" dtr-idx="15" dtr-ttl="Newcastle" href="https://sport.detik.com/sepakbola/profil-tim/liga-inggris/newcastle-united-fc"
 class="navbar-logo">
 <img src="https://cdn.detik.net.id/sportasset/soccerteamsset/logo_with_contestant_id/7vn2i2kd35zuetw6b38gw9jsz.png" alt="Newcastle">
 <span class="tooltip">
 Newcastle </span>
 </a>
 </li>
 <li class="nav__item liga_team_logo">
 <a dtr-evt="header" dtr-sec="" dtr-act="tim navbar" onclick="_pt(this)" dtr-idx="16" dtr-ttl="Leeds" href="https://sport.detik.com/sepakbola/profil-tim/liga-inggris/leeds-united-fc"
 class="navbar-logo">
 <img src="https://cdn.detik.net.id/sportasset/soccerteamsset/logo_with_contestant_id/48gk2hpqtsl6p9sx9kjhaydq4.png" alt="Leeds">
 <span class="tooltip">
 Leeds </span>
 </a>
 </li>
 <li class="nav__item liga_team_logo">
 <a dtr-evt="header" dtr-sec="" dtr-act="tim navbar" onclick="_pt(this)" dtr-idx="17" dtr-ttl="Everton" href="https://sport.detik.com/sepakbola/profil-tim/liga-inggris/everton-fc"
 class="navbar-logo">
 <img src="https://cdn.detik.net.id/sportasset/soccerteamsset/logo_with_contestant_id/ehd2iemqmschhj2ec0vayztzz.png" alt="Everton">
 <span class="tooltip">
 Everton </span>
 </a>
 </li>
 <li class="nav__item liga_team_logo">
 <a dtr-evt="header" dtr-sec="" dtr-act="tim navbar" onclick="_pt(this)" dtr-idx="18" dtr-ttl="Burnley" href="https://sport.detik.com/sepakbola/profil-tim/liga-inggris/burnley-fc"
 class="navbar-logo">
 <img src="https://cdn.detik.net.id/sportasset/soccerteamsset/logo_with_contestant_id/64bxxwu2mv2qqlv0monbkj1om.png" alt="Burnley">
 <span class="tooltip">
 Burnley </span>
 </a>
 </li>
 <li class="nav__item liga_team_logo">
 <a dtr-evt="header" dtr-sec="" dtr-act="tim navbar" onclick="_pt(this)" dtr-idx="19" dtr-ttl="Watford" href="https://sport.detik.com/sepakbola/profil-tim/liga-inggris/watford-fc"
 class="navbar-logo">
 <img src="https://cdn.detik.net.id/sportasset/soccerteamsset/logo_with_contestant_id/4t83rqbdbekinxl5fz2ygsyta.png" alt="Watford">
 <span class="tooltip">
 Watford </span>
 </a>
 </li>
 <li class="nav__item liga_team_logo">
 <a dtr-evt="header" dtr-sec="" dtr-act="tim navbar" onclick="_pt(this)" dtr-idx="20" dtr-ttl="Norwich" href="https://sport.detik.com/sepakbola/profil-tim/liga-inggris/norwich-city-fc"
 class="navbar-logo">
 <img src="https://cdn.detik.net.id/sportasset/soccerteamsset/logo_with_contestant_id/suz80crpy3anixyzccmu6jzp.png" alt="Norwich">
 <span class="tooltip">
 Norwich </span>
 </a>
 </li>
<!--cached @ 1649405347--><!--replaced--><!--0-->        </ul>
    </nav>
</div>
<script type="text/javascript">
$('select#wid_select_team').on('change', function (e) {
    var valueSelected = this.value;
    var _url = "https://sport.detik.com/sepakbola";
    $.ajax({
        type: "GET",
        url:_url+'/ajax/nav_team?param='+valueSelected,
        dataType: 'json',
        success: function(data){
            $('.liga_team_logo').remove();
            $('#liga_team').append(data.html);
        },
        error: function(jqxhr, status, exception){
            console.log(status);
            console.log(jqxhr.responseText);
        }
    })
});
</script>
    </div>
</header>

<!-- S:billboard --> <!-- E:billboard -->

<div class="container">
    <div class="page__header">
    <div class="page__breadcrumb">
        <a href="https://sport.detik.com/sepakbola/">Sepakbola</a>
        <a dtr-evt="breadcrumb" dtr-sec="breadcrumbkanal" dtr-act="breadcrumb kanal" onclick="_pt(this)" dtr-ttl="UEFA"            href="https://sport.detik.com/sepakbola/uefa">
            Uefa        </a>
    </div>
</div>

    <div class="grid-row content__bg mgt-16 ">
        <div class="column-8">
            <!-- S:topdetail --><center>
<div id="banner_top_detail">
<ins data-labelname="topdetail" data-revive-zoneid="198" data-revive-id="0cceecb9cae9f51a31123c541910d59b"></ins>
</div>
</center><!-- E:topdetail -->            <article class="detail">
                <div class="detail__header">
    
    
    <h1 class="detail__title">
        Saat Thomas Tuchel Ketus: Kamu Minta Saya Senyum?    </h1>

    <div class="detail__author">Afif Farhan - <span class="detail__label">Sepakbola</span></div>
    <div class="detail__date">Jumat, 08 Apr 2022 14:00 WIB</div>
    <script src="https://cdn.detik.net.id/libs/sharebox/js/shareBox.js?v=2022040815"></script>
<div class="flex-between share-box">
                    <div>
            <a dtr-evt="komentar top" dtr-sec="" dtr-act="tombol komentar" onclick="_pt(this)" dtr-ttl="komentar"                href="#comm1" class="btn btn--rounded btn--sm btn-comment komentar" data-url="https://sport.detik.com/sepakbola/uefa/d-6022811/saat-thomas-tuchel-ketus-kamu-minta-saya-senyum" data-title="Saat Thomas Tuchel Ketus: Kamu Minta Saya Senyum?" data-image="https://akcdn.detik.net.id/community/media/visual/2022/04/08/thomas-tuchel.jpeg" data-desc="Thomas Tuchel tidak bisa menyembunyikan kekecewaan kala Chelsea dihantam Real Madrid 1-3. Tuchel sensitif, jawab ketus pertanyaan jurnalis!" >
                <i class="icon icon-comment icon--xs"></i>
                <span>0 komentar</span>
            </a>
        </div>
                <div class="detail__share">
        BAGIKAN &nbsp;
        <a dtr-evt="share top" dtr-sec="" dtr-act="share facebook" onclick="_pt(this)" dtr-ttl="facebook"            href="#" class="icon-item icon-item__fb fb" data-url="https://sport.detik.com/sepakbola/uefa/d-6022811/saat-thomas-tuchel-ketus-kamu-minta-saya-senyum?utm_source=facebook&utm_campaign=detikcomsocmed&utm_medium=btn&utm_content=sepakbola" data-title="Saat Thomas Tuchel Ketus: Kamu Minta Saya Senyum?" data-image="https://akcdn.detik.net.id/community/media/visual/2022/04/08/thomas-tuchel.jpeg" data-desc="Thomas Tuchel tidak bisa menyembunyikan kekecewaan kala Chelsea dihantam Real Madrid 1-3. Tuchel sensitif, jawab ketus pertanyaan jurnalis!" >
            <i class="icon icon-facebook"></i>
        </a>
        <a dtr-evt="share top" dtr-sec="" dtr-act="share twitter" onclick="_pt(this)" dtr-ttl="twitter"            href="" class="icon-item icon-item__tw tw" data-url="https://sport.detik.com/sepakbola/uefa/d-6022811/saat-thomas-tuchel-ketus-kamu-minta-saya-senyum?utm_source=twitter&utm_campaign=detikcomsocmed&utm_medium=btn&utm_content=sepakbola" data-title="Saat Thomas Tuchel Ketus: Kamu Minta Saya Senyum?" data-image="https://akcdn.detik.net.id/community/media/visual/2022/04/08/thomas-tuchel.jpeg" data-desc="Thomas Tuchel tidak bisa menyembunyikan kekecewaan kala Chelsea dihantam Real Madrid 1-3. Tuchel sensitif, jawab ketus pertanyaan jurnalis!" >
            <i class="icon icon-twitter"></i>
        </a>
        <a dtr-evt="share top" dtr-sec="" dtr-act="share whatsapp" onclick="_pt(this)" dtr-ttl="whatsapp"            href="" class="icon-item icon-item__wa whatsap" data-url="https://sport.detik.com/sepakbola/uefa/d-6022811/saat-thomas-tuchel-ketus-kamu-minta-saya-senyum?utm_source=whatsapp&utm_campaign=detikcomsocmed&utm_medium=btn&utm_content=sepakbola" data-title="Saat Thomas Tuchel Ketus: Kamu Minta Saya Senyum?" data-image="https://akcdn.detik.net.id/community/media/visual/2022/04/08/thomas-tuchel.jpeg" data-desc="Thomas Tuchel tidak bisa menyembunyikan kekecewaan kala Chelsea dihantam Real Madrid 1-3. Tuchel sensitif, jawab ketus pertanyaan jurnalis!" >
            <i class="icon icon-whatsapp"></i>
        </a>
        <a dtr-evt="share top" dtr-sec="" dtr-act="share telegram" onclick="_pt(this)" dtr-ttl="telegram"            href="" class="icon-item icon-item__tele sh-telegram" data-url="https://sport.detik.com/sepakbola/uefa/d-6022811/saat-thomas-tuchel-ketus-kamu-minta-saya-senyum?utm_source=telegram&utm_campaign=detikcomsocmed&utm_medium=btn&utm_content=sepakbola" data-title="Saat Thomas Tuchel Ketus: Kamu Minta Saya Senyum?" data-image="https://akcdn.detik.net.id/community/media/visual/2022/04/08/thomas-tuchel.jpeg" data-desc="Thomas Tuchel tidak bisa menyembunyikan kekecewaan kala Chelsea dihantam Real Madrid 1-3. Tuchel sensitif, jawab ketus pertanyaan jurnalis!" >
            <i class="icon icon-telegram"></i>
        </a>
        <a dtr-evt="share top" dtr-sec="" dtr-act="copy link" onclick="_pt(this)" dtr-ttl="link"            href="" class="icon-item icon-item__link copy_url" data-url="https://sport.detik.com/sepakbola/uefa/d-6022811/saat-thomas-tuchel-ketus-kamu-minta-saya-senyum?utm_source=copy_url&utm_campaign=detikcomsocmed&utm_medium=btn&utm_content=sepakbola" data-title="Saat Thomas Tuchel Ketus: Kamu Minta Saya Senyum?" data-image="https://akcdn.detik.net.id/community/media/visual/2022/04/08/thomas-tuchel.jpeg" data-desc="Thomas Tuchel tidak bisa menyembunyikan kekecewaan kala Chelsea dihantam Real Madrid 1-3. Tuchel sensitif, jawab ketus pertanyaan jurnalis!" >
            <i class="icon icon-link"></i>
            <span class="notif">URL telah disalin</span>
        </a>
    </div>
</div>

<script type="text/javascript">
$(".copy_url").click(function() {
    $(this).children(".notif").addClass("url_copied");
    var dummy = document.createElement('input'),
    text = $(this).attr('data-url');
    document.body.appendChild(dummy);
    dummy.value = text;
    dummy.select();
    document.execCommand('copy');
    document.body.removeChild(dummy);
    setTimeout(function () {
        $(".notif").removeClass("url_copied");
    }, 1500);
    return false;
});

$(function() {
    $('a[href*="#"]:not([href="#"])').click(function() {
        if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
            if (target.length) {
                $('html, body').animate({
                    scrollTop: target.offset().top-53
                }, 1000);
                return false;
            }
        }
    });
});

var article = {
    idnews: 6022811,
    idkanal: 1033}

</script>
</div>
                    <div class="detail__media">
        <figure dtr-evt="cover image" dtr-sec="" dtr-act="cover image" onclick="_pt(this)"    class="detail__media-image">
     <img src="https://akcdn.detik.net.id/community/media/visual/2022/04/08/thomas-tuchel.jpeg?w=700&q=90" alt="Chelseas head coach Thomas Tuchel listens during a press conference ahead of Wednesdays Champions League first-leg quarterfinal soccer match between Chelsea and Real Madrid at Stamford Bridge stadium in London Tuesday, April 5, 2022. (AP Photo/Kirsty Wigglesworth)" title="Thomas Tuchel" class="p_img_zoomin img-zoomin" />    <figcaption class="detail__media-caption">Saat Thomas Tuchel Ketus: Kamu Minta Saya Senyum? (AP)</figcaption>
</figure>
    </div>

<!-- S:inbetweenimages --><!-- <div id="inbetween-video"></div> -->
<div id="inbetween"></div>
<script async src="https://securepubads.g.doubleclick.net/tag/js/gpt.js"></script>
<div id="gpt-passback">
  <script>
    window.googletag = window.googletag || {cmd: []};
    googletag.cmd.push(function() {
    googletag.defineSlot('/4905536/detik_desktop/sepakbola/inbetween', [1, 1], 'gpt-passback').addService(googletag.pubads());
    googletag.enableServices();
    googletag.display('gpt-passback');
    });
  </script>
</div><!-- E:inbetweenimages -->
<div class="detail__body itp_bodycontent_wrapper">
    <div class="detail__body-text itp_bodycontent">
        <strong>London</strong> - <p><a href="https://www.detik.com/tag/thomas-tuchel">Thomas Tuchel</a> tidak bisa menyembunyikan kekecewaan kala <a href="https://www.detik.com/tag/chelsea">Chelsea </a>dihantam <a href="https://www.detik.com/tag/real-madrid">Real Madrid</a> 1-3 di lanjutan Liga Champions. Tuchel sensitif, jawab ketus pertanyaan jurnalis!</p><p><a href="https://www.detik.com/tag/chelsea">Chelsea </a>kalah 1-3 dari Real Madrid di leg pertama babak <a href="https://www.detik.com/tag/perempatfinal-liga-champions">perempatfinal Liga Champions,</a> Kamis (7/4) dini hari WIB. Hat-trick Karim Benzema cuma bisa dibalas sekali oleh Kai Havertz.</p><p>Hasil itu bakal membuat The Blues harus kerja keras di leg kedua pekan depan di Santiago Bernabeu. Chelsea harus menang setidaknya 3-0 untuk lolos ke semifinal. Sedangkan Real Madrid, main seri 0-0 atau kalah 0-1 saja sudah cukup untuk lolos.</p><!--s:parallaxindetail--><div class="clearfix"></div><div id="parallax1">

<!-- /4905536/detik_desktop/sepakbola/parallax_detail -->
<div id='div-gpt-ad-1572510301589-0'style='width:500px; margin:auto;'>
  <script>
    googletag.cmd.push(function() { googletag.display('div-gpt-ad-1572510301589-0'); });
  </script>
</div>

</div>

<!--e:parallaxindetail--><table class="linksisip"><tbody><tr><td><div class="lihatjg"><strong>Baca juga: </strong><a data-label="List Berita" data-action="Berita Pilihan" data-category="Detil Artikel" href="https://sport.detik.com/sepakbola/uefa/d-6020830/chelsea-kalah-thomas-tuchel-kesal-banget-sampai-bilang-begini">Chelsea Kalah, Thomas Tuchel Kesal Banget Sampai Bilang Begini</a></div></td></tr></tbody></table><table class="linksisip"><tbody><tr><td><div class="lihatjg"><strong>Baca juga: </strong><a data-label="List Berita" data-action="Berita Pilihan" data-category="Detil Artikel" href="https://sport.detik.com/sepakbola/uefa/d-6021773/pertahanan-chelsea-lagi-keropos-banget">Pertahanan Chelsea Lagi Keropos Banget!</a></div></td></tr></tbody></table><p>Manajer Chelsea, <a href="https://www.detik.com/tag/thomas-tuchel">Thomas Tuchel</a> tidak bisa menyembunyikan perasaannya. Seorang wartawan <em>BT Sport</em> hendak mewawancarainya selepas laga, tapi langsung disambar dengan ketus!</p><p>"Apa yang Anda harapkan dari saya? Untuk tersenyum? Tentu saja saya sedih," kata Tuchel.</p><table class="linksisip"><tbody><tr><td><div class="lihatjg"><strong>Baca juga: </strong><a data-label="List Berita" data-action="Berita Pilihan" data-category="Detil Artikel" href="https://sport.detik.com/sepakbola/liga-inggris/d-6022128/stamford-bridge-kebanjiran-gol-di-2-laga-terakhir-tuchel-bingung">Stamford Bridge Kebanjiran Gol di 2 Laga Terakhir, Tuchel Bingung</a></div></td></tr></tbody></table><p><a href="https://www.detik.com/tag/thomas-tuchel">Thomas Tuchel</a> tentu lagi pusing tujuh keliling. <a href="https://www.detik.com/tag/chelsea">Chelsea </a>sedang kalah dua kali beruntun di Liga Inggris dan di Liga Champions dengan skor menohok 1-4 dari Brentfor dan 1-3 dari <a href="https://www.detik.com/tag/real-madrid">Real Madrid.</a></p><p>"Sekarang fokus saya ke Southampton (pertandingan Liga Inggris selanjutnya akhir pekan ini-red), bukan ke Real Madrid," tegas Tuchel.</p><p>"Saya pikir, tim harus mencerna dan memproses hasil-hasil belakangan ini. Jika kami tidak berubah ketika main lawan Southampton nanti, kami akan kalah lagi. Lalu tidak perlu mikir jauh-jauh (kalau tidak berubah juga), kami akan dibantai Madrid lagi," tutupnya.</p><p></p><table align="center" class="pic_artikel_sisip_table"><tbody><tr><td><div class="pic_artikel_sisip" align="center"><div class="pic"><img src="https://akcdn.detik.net.id/community/media/visual/2022/04/07/chelsea-vs-real-madrid-4.jpeg?w=3957" alt="Soccer Football - Champions League -  Quarter Final - First Leg - Chelsea v Real Madrid - Stamford Bridge, London, Britain - April 6, 2022 Real Madrid's Karim Benzema scores their first goal REUTERS/Tony Obrien" title="Chelsea vs Real Madrid" class="p_img_zoomin" />Chelsea sedang dalam tren buruk Foto: REUTERS/TONY OBRIEN</div></div></td></tr></tbody></table><p></p><p></p><table class="linksisip"><tbody><tr><td><div class="lihatjg"><strong>Baca juga: </strong><a data-label="List Berita" data-action="Berita Pilihan" data-category="Detil Artikel" href="https://sport.detik.com/sepakbola/uefa/d-6020390/jika-chelsea-begini-lagi-pekan-depan-siap-siap-dibantai-madrid">Jika Chelsea Begini Lagi Pekan Depan, Siap-siap Dibantai Madrid</a></div></td></tr></tbody></table><table class="linksisip"><tbody><tr><td><div class="lihatjg"><strong>Baca juga: </strong><a data-label="List Berita" data-action="Berita Pilihan" data-category="Detil Artikel" href="https://sport.detik.com/sepakbola/uefa/d-6021177/karena-nyawa-chelsea-belum-ngumpul">Karena Nyawa Chelsea Belum Ngumpul</a></div></td></tr></tbody></table>		        <strong>(aff/mrp)</strong>
        <div class="detail__body-tag mgt-16">
            <div class="nav">
                				<a class="nav__item" dtr-evt="tag" dtr-sec="" dtr-act="tag" onclick="_pt(this)" dtr-idx=" 1" dtr-ttl="thomas tuchel"  href="https://www.detik.com/tag/thomas-tuchel/">thomas tuchel</a>				<a class="nav__item" dtr-evt="tag" dtr-sec="" dtr-act="tag" onclick="_pt(this)" dtr-idx=" 2" dtr-ttl="chelsea"  href="https://www.detik.com/tag/chelsea/">chelsea</a>				<a class="nav__item" dtr-evt="tag" dtr-sec="" dtr-act="tag" onclick="_pt(this)" dtr-idx=" 3" dtr-ttl="real madrid"  href="https://www.detik.com/tag/real-madrid/">real madrid</a>				<a class="nav__item" dtr-evt="tag" dtr-sec="" dtr-act="tag" onclick="_pt(this)" dtr-idx=" 4" dtr-ttl="perempatfinal liga champions"  href="https://www.detik.com/tag/perempatfinal-liga-champions/">perempatfinal liga champions</a>            </div>
        </div>
    </div>
    <!-- S:skyscraper --><style>
.skybanner {
width: 160px;
}
</style>

<div class="skybanner">
<div class="sticky">
<div class="skybanner_container">

<!-- GPT AdSlot 1 for Ad unit 'detik_desktop/sepakbola/skyscrapper' ### Size: [[160,600],[120,600]] -->
<div id='div-gpt-ad-1923948-1'>
  <script>
    googletag.cmd.push(function() { googletag.display('div-gpt-ad-1923948-1'); });
  </script>
</div>
<!-- End AdSlot 1 -->


</div>
</div>
</div><!-- E:skyscraper --></div>

<!-- S:newstag --> <!-- E:newstag --><!-- S:hiddenquiz --> <!-- E:hiddenquiz -->
<script src="https://cdn.detik.net.id/libs/sharebox/js/shareBox.js?v=2022040815"></script>
<div class="flex-between share-box">
                    <div>
            <a dtr-evt="komentar bottom" dtr-sec="" dtr-act="tombol komentar" onclick="_pt(this)" dtr-ttl="komentar"                href="#comm1" class="btn btn--rounded btn--sm btn-comment komentar" data-url="https://sport.detik.com/sepakbola/uefa/d-6022811/saat-thomas-tuchel-ketus-kamu-minta-saya-senyum" data-title="Saat Thomas Tuchel Ketus: Kamu Minta Saya Senyum?" data-image="https://akcdn.detik.net.id/community/media/visual/2022/04/08/thomas-tuchel.jpeg" data-desc="Thomas Tuchel tidak bisa menyembunyikan kekecewaan kala Chelsea dihantam Real Madrid 1-3. Tuchel sensitif, jawab ketus pertanyaan jurnalis!" >
                <i class="icon icon-comment icon--xs"></i>
                <span>0 komentar</span>
            </a>
        </div>
                <div class="detail__share">
        BAGIKAN &nbsp;
        <a dtr-evt="share bottom" dtr-sec="" dtr-act="share facebook" onclick="_pt(this)" dtr-ttl="facebook"            href="#" class="icon-item icon-item__fb fb" data-url="https://sport.detik.com/sepakbola/uefa/d-6022811/saat-thomas-tuchel-ketus-kamu-minta-saya-senyum?utm_source=facebook&utm_campaign=detikcomsocmed&utm_medium=btn&utm_content=sepakbola" data-title="Saat Thomas Tuchel Ketus: Kamu Minta Saya Senyum?" data-image="https://akcdn.detik.net.id/community/media/visual/2022/04/08/thomas-tuchel.jpeg" data-desc="Thomas Tuchel tidak bisa menyembunyikan kekecewaan kala Chelsea dihantam Real Madrid 1-3. Tuchel sensitif, jawab ketus pertanyaan jurnalis!" >
            <i class="icon icon-facebook"></i>
        </a>
        <a dtr-evt="share bottom" dtr-sec="" dtr-act="share twitter" onclick="_pt(this)" dtr-ttl="twitter"            href="" class="icon-item icon-item__tw tw" data-url="https://sport.detik.com/sepakbola/uefa/d-6022811/saat-thomas-tuchel-ketus-kamu-minta-saya-senyum?utm_source=twitter&utm_campaign=detikcomsocmed&utm_medium=btn&utm_content=sepakbola" data-title="Saat Thomas Tuchel Ketus: Kamu Minta Saya Senyum?" data-image="https://akcdn.detik.net.id/community/media/visual/2022/04/08/thomas-tuchel.jpeg" data-desc="Thomas Tuchel tidak bisa menyembunyikan kekecewaan kala Chelsea dihantam Real Madrid 1-3. Tuchel sensitif, jawab ketus pertanyaan jurnalis!" >
            <i class="icon icon-twitter"></i>
        </a>
        <a dtr-evt="share bottom" dtr-sec="" dtr-act="share whatsapp" onclick="_pt(this)" dtr-ttl="whatsapp"            href="" class="icon-item icon-item__wa whatsap" data-url="https://sport.detik.com/sepakbola/uefa/d-6022811/saat-thomas-tuchel-ketus-kamu-minta-saya-senyum?utm_source=whatsapp&utm_campaign=detikcomsocmed&utm_medium=btn&utm_content=sepakbola" data-title="Saat Thomas Tuchel Ketus: Kamu Minta Saya Senyum?" data-image="https://akcdn.detik.net.id/community/media/visual/2022/04/08/thomas-tuchel.jpeg" data-desc="Thomas Tuchel tidak bisa menyembunyikan kekecewaan kala Chelsea dihantam Real Madrid 1-3. Tuchel sensitif, jawab ketus pertanyaan jurnalis!" >
            <i class="icon icon-whatsapp"></i>
        </a>
        <a dtr-evt="share bottom" dtr-sec="" dtr-act="share telegram" onclick="_pt(this)" dtr-ttl="telegram"            href="" class="icon-item icon-item__tele sh-telegram" data-url="https://sport.detik.com/sepakbola/uefa/d-6022811/saat-thomas-tuchel-ketus-kamu-minta-saya-senyum?utm_source=telegram&utm_campaign=detikcomsocmed&utm_medium=btn&utm_content=sepakbola" data-title="Saat Thomas Tuchel Ketus: Kamu Minta Saya Senyum?" data-image="https://akcdn.detik.net.id/community/media/visual/2022/04/08/thomas-tuchel.jpeg" data-desc="Thomas Tuchel tidak bisa menyembunyikan kekecewaan kala Chelsea dihantam Real Madrid 1-3. Tuchel sensitif, jawab ketus pertanyaan jurnalis!" >
            <i class="icon icon-telegram"></i>
        </a>
        <a dtr-evt="share bottom" dtr-sec="" dtr-act="copy link" onclick="_pt(this)" dtr-ttl="link"            href="" class="icon-item icon-item__link copy_url" data-url="https://sport.detik.com/sepakbola/uefa/d-6022811/saat-thomas-tuchel-ketus-kamu-minta-saya-senyum?utm_source=copy_url&utm_campaign=detikcomsocmed&utm_medium=btn&utm_content=sepakbola" data-title="Saat Thomas Tuchel Ketus: Kamu Minta Saya Senyum?" data-image="https://akcdn.detik.net.id/community/media/visual/2022/04/08/thomas-tuchel.jpeg" data-desc="Thomas Tuchel tidak bisa menyembunyikan kekecewaan kala Chelsea dihantam Real Madrid 1-3. Tuchel sensitif, jawab ketus pertanyaan jurnalis!" >
            <i class="icon icon-link"></i>
            <span class="notif">URL telah disalin</span>
        </a>
    </div>
</div>

<script type="text/javascript">
$(".copy_url").click(function() {
    $(this).children(".notif").addClass("url_copied");
    var dummy = document.createElement('input'),
    text = $(this).attr('data-url');
    document.body.appendChild(dummy);
    dummy.value = text;
    dummy.select();
    document.execCommand('copy');
    document.body.removeChild(dummy);
    setTimeout(function () {
        $(".notif").removeClass("url_copied");
    }, 1500);
    return false;
});

$(function() {
    $('a[href*="#"]:not([href="#"])').click(function() {
        if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
            if (target.length) {
                $('html, body').animate({
                    scrollTop: target.offset().top-53
                }, 1000);
                return false;
            }
        }
    });
});

var article = {
    idnews: 6022811,
    idkanal: 1033}

    setTimeout(function(){
        shareBox.run($('.share-box'));
        shareBox.countComment($('.komentar'));
    },500);
</script>

            </article>

            <!-- S:partner_box --><div class="newstag newstag2">
<!-- /4905536/detik_desktop/sepakbola/newstag -->
<div id='div-gpt-ad-1621407652950-0'>
  <script>
    googletag.cmd.push(function() { googletag.display('div-gpt-ad-1621407652950-0'); });
  </script>
</div>
</div><!-- E:partner_box -->
            <div class="box cb-berita-terkait">
    <div class="box__header">
        <div class="box__title">Berita Terkait</div>
    </div>
    <div id="bt_tkt" class="list-content list-content--column grid-row">
                <article class="list-content__item column-6">
            <h3 class="list-content__title">
                <a dtr-evt="berita terkait" dtr-sec="" dtr-act="berita terkait" onclick="_pt(this)" dtr-idx="1" dtr-id="6022225" dtr-ttl="Mason Mount Yakin Chelsea Bisa Comeback di Markas Madrid"                    href="https://sport.detik.com/sepakbola/uefa/d-6022225/mason-mount-yakin-chelsea-bisa-comeback-di-markas-madrid">
                    Mason Mount Yakin Chelsea Bisa Comeback di Markas Madrid                </a>
            </h3>
        </article>
                <!-- S:nativeberitaterkait --><ins data-labelname="nativeberitaterkait" data-revive-zoneid="4262" data-revive-id="0cceecb9cae9f51a31123c541910d59b"></ins><!-- E:nativeberitaterkait -->                        <article class="list-content__item column-6">
            <h3 class="list-content__title">
                <a dtr-evt="berita terkait" dtr-sec="" dtr-act="berita terkait" onclick="_pt(this)" dtr-idx="2" dtr-id="6022222" dtr-ttl="Simpati Courtois pada Mendy Usai Blunder Lawan Madrid"                    href="https://sport.detik.com/sepakbola/uefa/d-6022222/simpati-courtois-pada-mendy-usai-blunder-lawan-madrid">
                    Simpati Courtois pada Mendy Usai Blunder Lawan Madrid                </a>
            </h3>
        </article>
                        <article class="list-content__item column-6">
            <h3 class="list-content__title">
                <a dtr-evt="berita terkait" dtr-sec="" dtr-act="berita terkait" onclick="_pt(this)" dtr-idx="3" dtr-id="6022183" dtr-ttl="Ini Beda Lukaku dan Benzema?"                    href="https://sport.detik.com/sepakbola/uefa/d-6022183/ini-beda-lukaku-dan-benzema">
                    Ini Beda Lukaku dan Benzema?                </a>
            </h3>
        </article>
                        <article class="list-content__item column-6">
            <h3 class="list-content__title">
                <a dtr-evt="berita terkait" dtr-sec="" dtr-act="berita terkait" onclick="_pt(this)" dtr-idx="4" dtr-id="6022123" dtr-ttl="&#039;Lukaku Bukan Penyerang Elite&#039;"                    href="https://sport.detik.com/sepakbola/uefa/d-6022123/lukaku-bukan-penyerang-elite">
                    'Lukaku Bukan Penyerang Elite'                </a>
            </h3>
        </article>
                        <article class="list-content__item column-6">
            <h3 class="list-content__title">
                <a dtr-evt="berita terkait" dtr-sec="" dtr-act="berita terkait" onclick="_pt(this)" dtr-idx="5" dtr-id="6006318" dtr-ttl="Roman Abramovich, Miliarder yang Putus Kuliah tapi Sempat jadi Gubernur"                    href="https://www.detik.com/edu/edutainment/d-6006318/roman-abramovich-miliarder-yang-putus-kuliah-tapi-sempat-jadi-gubernur">
                    Roman Abramovich, Miliarder yang Putus Kuliah tapi Sempat jadi Gubernur                </a>
            </h3>
        </article>
                        <article class="list-content__item column-6">
            <h3 class="list-content__title">
                <a dtr-evt="berita terkait" dtr-sec="" dtr-act="berita terkait" onclick="_pt(this)" dtr-idx="6" dtr-id="6004617" dtr-ttl="Tarik Tambang Lawan Harimau, Bek Chelsea Dirujak Pecinta Satwa"                    href="https://travel.detik.com/travel-news/d-6004617/tarik-tambang-lawan-harimau-bek-chelsea-dirujak-pecinta-satwa">
                    Tarik Tambang Lawan Harimau, Bek Chelsea Dirujak Pecinta Satwa                </a>
            </h3>
        </article>
                        <article class="list-content__item column-6">
            <h3 class="list-content__title">
                <a dtr-evt="berita terkait" dtr-sec="" dtr-act="berita terkait" onclick="_pt(this)" dtr-idx="7" dtr-id="5992818" dtr-ttl="Momen-momen Barcelona Pecundangi Madrid"                    href="https://sport.detik.com/detiktv/d-5992818/momen-momen-barcelona-pecundangi-madrid">
                    Momen-momen Barcelona Pecundangi Madrid                </a>
            </h3>
        </article>
                        <article class="list-content__item column-6">
            <h3 class="list-content__title">
                <a dtr-evt="berita terkait" dtr-sec="" dtr-act="berita terkait" onclick="_pt(this)" dtr-idx="8" dtr-id="5990589" dtr-ttl="Kocak, Bastianini Diminta Prediksi El Clasico Malah Tulis Juve Vs Madrid"                    href="https://sport.detik.com/detiktv/d-5990589/kocak-bastianini-diminta-prediksi-el-clasico-malah-tulis-juve-vs-madrid">
                    Kocak, Bastianini Diminta Prediksi El Clasico Malah Tulis Juve Vs Madrid                </a>
            </h3>
        </article>
                    </div>
</div>
<script type="text/javascript">
    $(document).on('DOMSubtreeModified', '#bt_tkt', function() {
        var artikel_terkait = $("#bt_tkt").find('article');
        if (artikel_terkait.length > 8) {
            artikel_terkait.last().hide()
        }
    })
</script>

            <div d-widget="newsfeed_recommendation" d-recommendation></div>

            <div class="box cb-artikel-lainnya">
    <div class="box__header">
        <div class="box__title">Berita detikcom Lainnya</div>
    </div>
    <div id="bt_l" class="grid-row list-content list-content--column">
                
                <article class="list-content__item column-3">
            <div class="media media--image-radius">
                <div class="media__image">
                    <a dtr-evt="box berita detikcom lainnya" dtr-sec="" dtr-act="artikel" onclick="_pt(this)" dtr-idx="1" dtr-id="6022978" dtr-ttl="Meghan Markle Dikritik karena Patenkan Kata Archetype, Apa Artinya?"                        href="https://wolipop.detik.com/entertainment-news/d-6022978/meghan-markle-dikritik-karena-patenkan-kata-archetype-apa-artinya"
                        class="media__link">
                        <span class="ratiobox ratiobox--16-9 lqd">
                             <img src="https://akcdn.detik.net.id/community/media/visual/2021/09/24/pangeran-harry-dan-meghan-markle-di-new-york-city-3_169.jpeg?w=200&q=90" alt="Meghan Markle Dikritik karena Patenkan Kata Archetype, Apa Artinya?" title="Meghan Markle Dikritik karena Patenkan Kata Archetype, Apa Artinya?" class="" />                        </span>
                    </a>
                </div>
                <div class="media__text">
                    <div class="media__subtitle">Wolipop</div>
                    <div class="media__title">
                        <a dtr-evt="box berita detikcom lainnya" dtr-sec="" dtr-act="artikel" onclick="_pt(this)" dtr-idx="1" dtr-id="6022978" dtr-ttl="Meghan Markle Dikritik karena Patenkan Kata Archetype, Apa Artinya?"                            href="https://wolipop.detik.com/entertainment-news/d-6022978/meghan-markle-dikritik-karena-patenkan-kata-archetype-apa-artinya"
                            class="media__link">
                            Meghan Markle Dikritik karena Patenkan Kata Archetype, Apa Artinya?                        </a>
                    </div>
                </div>
            </div>
        </article>
                
                <article class="list-content__item column-3">
            <div class="media media--image-radius">
                <div class="media__image">
                    <a dtr-evt="box berita detikcom lainnya" dtr-sec="" dtr-act="artikel" onclick="_pt(this)" dtr-idx="2" dtr-id="6022547" dtr-ttl="Begini Kondisi Pasien Varian XE Pertama di India, Alami Gejala Apa?"                        href="https://health.detik.com/berita-detikhealth/d-6022547/begini-kondisi-pasien-varian-xe-pertama-di-india-alami-gejala-apa"
                        class="media__link">
                        <span class="ratiobox ratiobox--16-9 lqd">
                             <img src="https://akcdn.detik.net.id/community/media/visual/2021/05/04/potret-antrean-ambulans-pembawa-pasien-corona-di-india-5_169.jpeg?w=200&q=90" alt="Begini Kondisi Pasien Varian XE Pertama di India, Alami Gejala Apa?" title="Begini Kondisi Pasien Varian XE Pertama di India, Alami Gejala Apa?" class="" />                        </span>
                    </a>
                </div>
                <div class="media__text">
                    <div class="media__subtitle">detikHealth</div>
                    <div class="media__title">
                        <a dtr-evt="box berita detikcom lainnya" dtr-sec="" dtr-act="artikel" onclick="_pt(this)" dtr-idx="2" dtr-id="6022547" dtr-ttl="Begini Kondisi Pasien Varian XE Pertama di India, Alami Gejala Apa?"                            href="https://health.detik.com/berita-detikhealth/d-6022547/begini-kondisi-pasien-varian-xe-pertama-di-india-alami-gejala-apa"
                            class="media__link">
                            Begini Kondisi Pasien Varian XE Pertama di India, Alami Gejala Apa?                        </a>
                    </div>
                </div>
            </div>
        </article>
                        <!-- S:nativenewsfeed1 --> <!-- E:nativenewsfeed1 -->        
                <article class="list-content__item column-3">
            <div class="media media--image-radius">
                <div class="media__image">
                    <a dtr-evt="box berita detikcom lainnya" dtr-sec="" dtr-act="artikel" onclick="_pt(this)" dtr-idx="3" dtr-id="6022846" dtr-ttl="Warga Gugat ke MK Minta Jabatan Anies Diperpanjang, Wagub DKI Bilang Begini"                        href="https://news.detik.com/berita/d-6022846/warga-gugat-ke-mk-minta-jabatan-anies-diperpanjang-wagub-dki-bilang-begini"
                        class="media__link">
                        <span class="ratiobox ratiobox--16-9 lqd">
                             <img src="https://akcdn.detik.net.id/community/media/visual/2022/03/17/ahmad-riza-patria_169.jpeg?w=200&q=90" alt="Warga Gugat ke MK Minta Jabatan Anies Diperpanjang, Wagub DKI Bilang Begini" title="Warga Gugat ke MK Minta Jabatan Anies Diperpanjang, Wagub DKI Bilang Begini" class="" />                        </span>
                    </a>
                </div>
                <div class="media__text">
                    <div class="media__subtitle">detikNews</div>
                    <div class="media__title">
                        <a dtr-evt="box berita detikcom lainnya" dtr-sec="" dtr-act="artikel" onclick="_pt(this)" dtr-idx="3" dtr-id="6022846" dtr-ttl="Warga Gugat ke MK Minta Jabatan Anies Diperpanjang, Wagub DKI Bilang Begini"                            href="https://news.detik.com/berita/d-6022846/warga-gugat-ke-mk-minta-jabatan-anies-diperpanjang-wagub-dki-bilang-begini"
                            class="media__link">
                            Warga Gugat ke MK Minta Jabatan Anies Diperpanjang, Wagub DKI Bilang Begini                        </a>
                    </div>
                </div>
            </div>
        </article>
                
                <article class="list-content__item column-3">
            <div class="media media--image-radius">
                <div class="media__image">
                    <a dtr-evt="box berita detikcom lainnya" dtr-sec="" dtr-act="artikel" onclick="_pt(this)" dtr-idx="4" dtr-id="6022835" dtr-ttl="Marc Marquez Sempat Takut Usai Kecelakaan di MotoGP Mandalika"                        href="https://sport.detik.com/moto-gp/d-6022835/marc-marquez-sempat-takut-usai-kecelakaan-di-motogp-mandalika"
                        class="media__link">
                        <span class="ratiobox ratiobox--16-9 lqd">
                             <img src="https://akcdn.detik.net.id/community/media/visual/2022/03/22/marc-marquez-2_169.jpeg?w=200&q=90" alt="Marc Marquez Sempat Takut Usai Kecelakaan di MotoGP Mandalika" title="Marc Marquez Sempat Takut Usai Kecelakaan di MotoGP Mandalika" class="" />                        </span>
                    </a>
                </div>
                <div class="media__text">
                    <div class="media__subtitle">detikSport</div>
                    <div class="media__title">
                        <a dtr-evt="box berita detikcom lainnya" dtr-sec="" dtr-act="artikel" onclick="_pt(this)" dtr-idx="4" dtr-id="6022835" dtr-ttl="Marc Marquez Sempat Takut Usai Kecelakaan di MotoGP Mandalika"                            href="https://sport.detik.com/moto-gp/d-6022835/marc-marquez-sempat-takut-usai-kecelakaan-di-motogp-mandalika"
                            class="media__link">
                            Marc Marquez Sempat Takut Usai Kecelakaan di MotoGP Mandalika                        </a>
                    </div>
                </div>
            </div>
        </article>
                
                <!-- S:nativenewsfeed2 --> <!-- E:nativenewsfeed2 -->                <article class="list-content__item column-3">
            <div class="media media--image-radius">
                <div class="media__image">
                    <a dtr-evt="box berita detikcom lainnya" dtr-sec="" dtr-act="artikel" onclick="_pt(this)" dtr-idx="5" dtr-id="6022784" dtr-ttl="Waspada! Malware Android Pembobol Mobile Banking Kembali Mengintai"                        href="https://inet.detik.com/security/d-6022784/waspada-malware-android-pembobol-mobile-banking-kembali-mengintai"
                        class="media__link">
                        <span class="ratiobox ratiobox--16-9 lqd">
                             <img src="https://akcdn.detik.net.id/community/media/visual/2022/02/07/ilustrasi-serangan-malware_169.jpeg?w=200&q=90" alt="Waspada! Malware Android Pembobol Mobile Banking Kembali Mengintai" title="Waspada! Malware Android Pembobol Mobile Banking Kembali Mengintai" class="" />                        </span>
                    </a>
                </div>
                <div class="media__text">
                    <div class="media__subtitle">detikInet</div>
                    <div class="media__title">
                        <a dtr-evt="box berita detikcom lainnya" dtr-sec="" dtr-act="artikel" onclick="_pt(this)" dtr-idx="5" dtr-id="6022784" dtr-ttl="Waspada! Malware Android Pembobol Mobile Banking Kembali Mengintai"                            href="https://inet.detik.com/security/d-6022784/waspada-malware-android-pembobol-mobile-banking-kembali-mengintai"
                            class="media__link">
                            Waspada! Malware Android Pembobol Mobile Banking Kembali Mengintai                        </a>
                    </div>
                </div>
            </div>
        </article>
                
                <article class="list-content__item column-3">
            <div class="media media--image-radius">
                <div class="media__image">
                    <a dtr-evt="box berita detikcom lainnya" dtr-sec="" dtr-act="artikel" onclick="_pt(this)" dtr-idx="6" dtr-id="6022632" dtr-ttl="Mie Ayam Bangka Asan: Gurih Nikmat Mie Ayam Bangka Legendaris di Cipinang"                        href="https://food.detik.com/rumah-makan/d-6022632/mie-ayam-bangka-asan-gurih-nikmat-mie-ayam-bangka-legendaris-di-cipinang"
                        class="media__link">
                        <span class="ratiobox ratiobox--16-9 lqd">
                             <img src="https://akcdn.detik.net.id/community/media/visual/2022/04/08/mie-ayam-bangka-asan_169.jpeg?w=200&q=90" alt="Mie Ayam Bangka Asan: Gurih Nikmat Mie Ayam Bangka Legendaris di Cipinang" title="Mie Ayam Bangka Asan: Gurih Nikmat Mie Ayam Bangka Legendaris di Cipinang" class="" />                        </span>
                    </a>
                </div>
                <div class="media__text">
                    <div class="media__subtitle">detikFood</div>
                    <div class="media__title">
                        <a dtr-evt="box berita detikcom lainnya" dtr-sec="" dtr-act="artikel" onclick="_pt(this)" dtr-idx="6" dtr-id="6022632" dtr-ttl="Mie Ayam Bangka Asan: Gurih Nikmat Mie Ayam Bangka Legendaris di Cipinang"                            href="https://food.detik.com/rumah-makan/d-6022632/mie-ayam-bangka-asan-gurih-nikmat-mie-ayam-bangka-legendaris-di-cipinang"
                            class="media__link">
                            Mie Ayam Bangka Asan: Gurih Nikmat Mie Ayam Bangka Legendaris di Cipinang                        </a>
                    </div>
                </div>
            </div>
        </article>
                
                <article class="list-content__item column-3">
            <div class="media media--image-radius">
                <div class="media__image">
                    <a dtr-evt="box berita detikcom lainnya" dtr-sec="" dtr-act="artikel" onclick="_pt(this)" dtr-idx="7" dtr-id="6022836" dtr-ttl="Sri Lanka Dilanda Krisis-Utang Bengkak, Ternyata Ini Biang Keroknya"                        href="https://finance.detik.com/berita-ekonomi-bisnis/d-6022836/sri-lanka-dilanda-krisis-utang-bengkak-ternyata-ini-biang-keroknya"
                        class="media__link">
                        <span class="ratiobox ratiobox--16-9 lqd">
                             <img src="https://akcdn.detik.net.id/community/media/visual/2022/04/01/mencekam-demo-krisis-ekonomi-di-sri-lanka-diwarnai-aksi-bakar-bus-4_169.jpeg?w=200&q=90" alt="Sri Lanka Dilanda Krisis-Utang Bengkak, Ternyata Ini Biang Keroknya" title="Sri Lanka Dilanda Krisis-Utang Bengkak, Ternyata Ini Biang Keroknya" class="" />                        </span>
                    </a>
                </div>
                <div class="media__text">
                    <div class="media__subtitle">detikFinance</div>
                    <div class="media__title">
                        <a dtr-evt="box berita detikcom lainnya" dtr-sec="" dtr-act="artikel" onclick="_pt(this)" dtr-idx="7" dtr-id="6022836" dtr-ttl="Sri Lanka Dilanda Krisis-Utang Bengkak, Ternyata Ini Biang Keroknya"                            href="https://finance.detik.com/berita-ekonomi-bisnis/d-6022836/sri-lanka-dilanda-krisis-utang-bengkak-ternyata-ini-biang-keroknya"
                            class="media__link">
                            Sri Lanka Dilanda Krisis-Utang Bengkak, Ternyata Ini Biang Keroknya                        </a>
                    </div>
                </div>
            </div>
        </article>
                
                <article class="list-content__item column-3">
            <div class="media media--image-radius">
                <div class="media__image">
                    <a dtr-evt="box berita detikcom lainnya" dtr-sec="" dtr-act="artikel" onclick="_pt(this)" dtr-idx="8" dtr-id="6022811" dtr-ttl="Saat Thomas Tuchel Ketus: Kamu Minta Saya Senyum?"                        href="https://sport.detik.com/sepakbola/uefa/d-6022811/saat-thomas-tuchel-ketus-kamu-minta-saya-senyum"
                        class="media__link">
                        <span class="ratiobox ratiobox--16-9 lqd">
                             <img src="https://akcdn.detik.net.id/community/media/visual/2022/04/08/thomas-tuchel_169.jpeg?w=200&q=90" alt="Saat Thomas Tuchel Ketus: Kamu Minta Saya Senyum?" title="Saat Thomas Tuchel Ketus: Kamu Minta Saya Senyum?" class="" />                        </span>
                    </a>
                </div>
                <div class="media__text">
                    <div class="media__subtitle">Sepakbola</div>
                    <div class="media__title">
                        <a dtr-evt="box berita detikcom lainnya" dtr-sec="" dtr-act="artikel" onclick="_pt(this)" dtr-idx="8" dtr-id="6022811" dtr-ttl="Saat Thomas Tuchel Ketus: Kamu Minta Saya Senyum?"                            href="https://sport.detik.com/sepakbola/uefa/d-6022811/saat-thomas-tuchel-ketus-kamu-minta-saya-senyum"
                            class="media__link">
                            Saat Thomas Tuchel Ketus: Kamu Minta Saya Senyum?                        </a>
                    </div>
                </div>
            </div>
        </article>
            </div>
</div>
<script type="text/javascript">
    $(document).on('DOMSubtreeModified', '#bt_l', function() {
        var btl = $("#bt_l").find('article');
        if (btl.length == 10) {
            btl.last().hide()
            btl.eq(-2).hide()
        } else if (btl.length == 9) {
            btl.last().hide()
        }
    })
</script>

            <div class="box komentar_box" id="comm1">
                <!--S:KOMENTAR-->
    <script src="https://cdn.detik.net.id/libs/newcomment/js/xcomponent.frame.min.js?2022040815"></script>
    <script src="https://cdn.detik.net.id/libs/newcomment/js/bridge.js?2022040815"></script>

    
    <div id="thecomment2" class="detail_area"></div>

    <!-- S: NEW PRO KONTRA -->
    <script type="text/javascript">

        DtkXComponent.render({
            url_dtk: "https://sport.detik.com/sepakbola/uefa/d-6022811/saat-thomas-tuchel-ketus-kamu-minta-saya-senyum",
            identifier: 6022811,
            group: 1033,
            date: "08-04-2022",
            title: "Saat Thomas Tuchel Ketus: Kamu Minta Saya Senyum?",
            appId: 27,
            url_share: "https://sport.detik.com/sepakbola/uefa/d-6022811/saat-thomas-tuchel-ketus-kamu-minta-saya-senyum",
            prefix: "dtk",
                        prokontra: 0,
            showhide: 0,
            kanalAds: "detik_sepakbola",
            envAds: "desktop",
            onLogin,
            onResize,
            onScroll,
            onAlert,
            data_oa
        },'#thecomment2');

                    try {
                $('#thecomment2').attrchange({
                    callback: function (e) {
                        $(document.body).trigger("sticky_kit:recalc");
                    }
                }).resizable();
            } catch (e) {} finally {}
        
    </script>
<!-- E: NEW PRO KONTRA -->
<!--E:KOMENTAR-->
            </div>
        </div>

        <div class="column-4">
            
                        <!-- S:mediumrectangle1 -->
<div class="box-ads text-center mr1" id="mr1">
<div id='div-gpt-ad-1534407897257-0'>
<script>
googletag.cmd.push(function() { googletag.display('div-gpt-ad-1534407897257-0'); });
</script>
</div>
</div>

<script>
function stickyMR1(){

var MR_1 = document.getElementById("mr1");
if (MR_1){
var billsticky= ($('#billboard_banner').hasClass('BB_sticky'));

var stickyMR1 = MR_1.getBoundingClientRect().top;
var scrollMR1 = $(window).scrollTop();
var TotalHeight1 = ((typeof $('.top_banner_bar').outerHeight() === 'undefined') ? 0 : $('.top_banner_bar').outerHeight())
+ ((typeof bill_gpt.outerHeight() === 'undefined')? 0 : bill_gpt.outerHeight())+((typeof $('.dtkframebar').outerHeight() === 'undefined') ? 0 : $('.dtkframebar').outerHeight()) ;
if(billsticky == false){
if ($("#mr1").length && (stickyMR1 ) <= TotalHeight1){
$("#mr1").addClass('mr1_sticky');
if ($('#billboard_banner').hasClass('BB_sticky') && ($('.top_banner_bar').length > 0) ){
$("#mr1").css({top: '375px'});
}
else if (($(".mr1").height()) > 250) {
$("#mr1").css({top: '46px'});
$("#mr2").css({top: '650px'});
}
else if ($('.top_banner_bar').length > 0) {
$("#mr1").css({top: '112px'});
}
else{
$("#mr1").css({top: '46px'});
}
setTimeout(function(){
$('.mr1').removeClass('mr1_sticky');
$('.mr1').removeAttr('id');
$('.mr1').removeAttr('style');
}, 2000);
}

}
}
}
</script>

<!--NO_REFRESH--><!-- E:mediumrectangle1 -->            <!-- S:mediumrectangle2 --><div class="mr2" style="height: 350px">
<div class="mr2-scrollpage-container">
<div class="mr2-scrollpage-height" style="height: 350px">
<div class="mr2-scrollpage-box">
<div class="mr2-scrollpage-top">
<div class="mr2-scrollpage-banner">
<!-- /4905536/detik_desktop/sepakbola/medium_rectangle2 -->
        <div id='div-gpt-ad-1534408024708-0'>
        <script>
            googletag.cmd.push(function() { googletag.display('div-gpt-ad-1534408024708-0'); });
        </script>
    </div>
</div>
</div>
</div>
</div>
</div>
</div>

<style>@import url("https://cdnstatic.detik.com/live/_rmbassets/mr2-scrollpage/scrollpage.css");
</style><!-- E:mediumrectangle2 -->                        <div class="box cb-mostpop">
<div class="box__header">
    <h2 class="box__title">Berita Terpopuler</h2>
</div>
<div class="list-content">

            <article class="list-content__item">
            <div class="media">
                <div class="media__text text-list">
                    <span class="text-list__data ">#1</span>
                    <h3 class="media__title">
                        <a dtr-evt="box artikel terpopuler" dtr-sec="" dtr-act="terpopuler" onclick="_pt(this)" dtr-idx="1" dtr-id="6022550" dtr-ttl="Rooney Balas Sindiran &#039;Iri&#039; dari Cristiano Ronaldo"                            href="https://sport.detik.com/sepakbola/liga-inggris/d-6022550/rooney-balas-sindiran-iri-dari-cristiano-ronaldo"
                            class="media__link">
                            Rooney Balas Sindiran 'Iri' dari Cristiano Ronaldo                        </a>
                    </h3>
                </div>
            </div>
        </article>
            <article class="list-content__item">
            <div class="media">
                <div class="media__text text-list">
                    <span class="text-list__data ">#2</span>
                    <h3 class="media__title">
                        <a dtr-evt="box artikel terpopuler" dtr-sec="" dtr-act="terpopuler" onclick="_pt(this)" dtr-idx="2" dtr-id="6022577" dtr-ttl="Liverpool Berbahaya, Man City Tak Boleh Bikin Kesalahan"                            href="https://sport.detik.com/sepakbola/liga-inggris/d-6022577/liverpool-berbahaya-man-city-tak-boleh-bikin-kesalahan"
                            class="media__link">
                            Liverpool Berbahaya, Man City Tak Boleh Bikin Kesalahan                        </a>
                    </h3>
                </div>
            </div>
        </article>
            <article class="list-content__item">
            <div class="media">
                <div class="media__text text-list">
                    <span class="text-list__data ">#3</span>
                    <h3 class="media__title">
                        <a dtr-evt="box artikel terpopuler" dtr-sec="" dtr-act="terpopuler" onclick="_pt(this)" dtr-idx="3" dtr-id="6022750" dtr-ttl="Barcelona Cuma Seri, Xavi Salahkan Rumput"                            href="https://sport.detik.com/sepakbola/uefa/d-6022750/barcelona-cuma-seri-xavi-salahkan-rumput"
                            class="media__link">
                            Barcelona Cuma Seri, Xavi Salahkan Rumput                        </a>
                    </h3>
                </div>
            </div>
        </article>
            <article class="list-content__item">
            <div class="media">
                <div class="media__text text-list">
                    <span class="text-list__data ">#4</span>
                    <h3 class="media__title">
                        <a dtr-evt="box artikel terpopuler" dtr-sec="" dtr-act="terpopuler" onclick="_pt(this)" dtr-idx="4" dtr-id="6022811" dtr-ttl="Saat Thomas Tuchel Ketus: Kamu Minta Saya Senyum?"                            href="https://sport.detik.com/sepakbola/uefa/d-6022811/saat-thomas-tuchel-ketus-kamu-minta-saya-senyum"
                            class="media__link">
                            Saat Thomas Tuchel Ketus: Kamu Minta Saya Senyum?                        </a>
                    </h3>
                </div>
            </div>
        </article>
            <article class="list-content__item">
            <div class="media">
                <div class="media__text text-list">
                    <span class="text-list__data ">#5</span>
                    <h3 class="media__title">
                        <a dtr-evt="box artikel terpopuler" dtr-sec="" dtr-act="terpopuler" onclick="_pt(this)" dtr-idx="5" dtr-id="6022708" dtr-ttl="Ngeri! Fans Perang Petasan dalam Laga Conference League"                            href="https://sport.detik.com/sepakbola/gila-bola/d-6022708/ngeri-fans-perang-petasan-dalam-laga-conference-league"
                            class="media__link">
                            Ngeri! Fans Perang Petasan dalam Laga Conference League                        </a>
                    </h3>
                </div>
            </div>
        </article>
    
    <div class="text-center pdt-12 pdb-12">
        <a dtr-evt="box artikel terpopuler" dtr-sec="lihatselengkapnya" dtr-act="lihat selengkapnya" onclick="_pt(this)"            href="https://www.detik.com/terpopuler/sepakbola"
            class="btn btn--default btn--default-next btn--md">
            Lihat Selengkapnya <i class="icon icon-arrow-right"></i>
        </a>
    </div>
</div>
</div>
            <div d-widget="most_commented" d-params=""><div class="dwidget_desktop ph-list"><div class="ph-item ph-title"><div class="ph-row"><div class="ph-col-12 big"></div></div></div> <div class="ph-item"><div class="ph-col-12"><div class="ph-row"><div class="ph-col-8 big"></div><div class="ph-col-4 empty"></div></div></div></div> <div class="ph-item"><div class="ph-col-12"><div class="ph-row"><div class="ph-col-8 big"></div><div class="ph-col-4 empty"></div></div></div></div> <div class="ph-item"><div class="ph-col-12"><div class="ph-row"><div class="ph-col-8 big"></div><div class="ph-col-4 empty"></div></div></div></div> </div> <img style="display:none"/></div>
            <div d-widget="football_moments" d-params=""><div class="dwidget_desktop ph-horizontal"><div class="ph-item"><div class="ph-col-4"><div class="ph-picture"></div></div><div><div class="ph-row"><div class="ph-col-12 big"></div><div class="ph-col-8"></div><div class="ph-col-4 empty"></div></div></div></div></div><img style="display:none" /></div>
                        <!-- S:mediumrectangle3 -->

<div class="box-ads text-center">

<!-- /4905536/detik_desktop/sepakbola/medium_rectangle3 -->
<div id='div-gpt-ad-1556897709376-0'>
  <script>
    googletag.cmd.push(function() { googletag.display('div-gpt-ad-1556897709376-0'); });
  </script>
</div>


</div><!-- E:mediumrectangle3 -->            <!-- S:mediumrectangle4 --> <!-- E:mediumrectangle4 -->                        <div d-widget="foto_detail" d-params=""><div class="dwidget_desktop ph-horizontal"><div class="ph-item"><div class="ph-col-4"><div class="ph-picture"></div></div><div><div class="ph-row"><div class="ph-col-12 big"></div><div class="ph-col-8"></div><div class="ph-col-4 empty"></div></div></div><div class="ph-col-4"><div class="ph-picture"></div></div><div><div class="ph-row"><div class="ph-col-12 big"></div><div class="ph-col-8"></div><div class="ph-col-4 empty"></div></div></div></div><div class="ph-item"><div class="ph-col-4"><div class="ph-picture"></div></div><div><div class="ph-row"><div class="ph-col-12 big"></div><div class="ph-col-8"></div><div class="ph-col-4 empty"></div></div></div><div class="ph-col-4"><div class="ph-picture"></div></div><div><div class="ph-row"><div class="ph-col-12 big"></div><div class="ph-col-8"></div><div class="ph-col-4 empty"></div></div></div></div></div><img style="display:none" /></div>
            <div d-widget="video_detail" d-params=""><div class="dwidget_desktop ph-horizontal"><div class="ph-item"><div class="ph-col-4"><div class="ph-picture"></div></div><div><div class="ph-row"><div class="ph-col-12 big"></div><div class="ph-col-8"></div><div class="ph-col-4 empty"></div></div></div><div class="ph-col-4"><div class="ph-picture"></div></div><div><div class="ph-row"><div class="ph-col-12 big"></div><div class="ph-col-8"></div><div class="ph-col-4 empty"></div></div></div></div><div class="ph-item"><div class="ph-col-4"><div class="ph-picture"></div></div><div><div class="ph-row"><div class="ph-col-12 big"></div><div class="ph-col-8"></div><div class="ph-col-4 empty"></div></div></div><div class="ph-col-4"><div class="ph-picture"></div></div><div><div class="ph-row"><div class="ph-col-12 big"></div><div class="ph-col-8"></div><div class="ph-col-4 empty"></div></div></div></div></div><img style="display:none" /></div>
        
                        <!-- S:mediumrectangle5 --><!-- E:mediumrectangle5 -->            <!-- S:mediumrectangle6 --><!-- E:mediumrectangle6 -->                        <div class="sticky mgb-24">
                <div d-widget="most_popular" d-params=""><div class="dwidget_desktop ph-list"><div class="ph-item ph-title"><div class="ph-row"><div class="ph-col-12 big"></div></div></div> <div class="ph-item"><div class="ph-col-12"><div class="ph-row"><div class="ph-col-8 big"></div><div class="ph-col-4 empty"></div></div></div></div> <div class="ph-item"><div class="ph-col-12"><div class="ph-row"><div class="ph-col-8 big"></div><div class="ph-col-4 empty"></div></div></div></div> <div class="ph-item"><div class="ph-col-12"><div class="ph-row"><div class="ph-col-8 big"></div><div class="ph-col-4 empty"></div></div></div></div> <div class="ph-item"><div class="ph-col-12"><div class="ph-row"><div class="ph-col-8 big"></div><div class="ph-col-4 empty"></div></div></div></div> <div class="ph-item"><div class="ph-col-12"><div class="ph-row"><div class="ph-col-8 big"></div><div class="ph-col-4 empty"></div></div></div></div> </div> <img style="display:none"/></div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" src="https://cdn.detik.net.id/assets/js/detik.js?v=2022040815"></script>
<script type="text/javascript">
    plugins({
        "modal_box":false,
        "iframe_resizer":false,
        "autocomplete":false,
        "liquid_img":true,
        "datepicker":true,
        "tabs":false,
        "tooltips":false,
        "zoom_img":({status:true})
    });

     link_copied();
    $("[d-time]").detikTime();
</script>
<footer>
	<div class="footer">
		<div class="footer__border"></div>
		<div class="container">
			<div class="grid-row footer__grid">
				<div class="column-3 text-center">
					<div class="footer__logo">
						<a dtr-evt="footer" dtr-sec="logodetik" dtr-act="logo detik" onclick="_pt(this)"							href="https://www.detik.com" >
							<img src="https://cdn.detik.net.id/detikcom/images/logo-detik.png" alt="Logo detikcom" title="Logo detikcom">
						</a>
					</div>
					<div class="footer-title footer-title__partof">part of <img src="https://cdn.detik.net.id/assets/images/logo_detiknetwork.png?v=2022040815" alt="img-alt" title="img-title"></div>
					<div class="footer__social">
						<div class="footer-title footer-title__connect">Connect With Us</div>
						<a dtr-evt="footer" dtr-sec="sharefooter" dtr-act="share footer" onclick="_pt(this)" dtr-ttl="facebook"							target="_blank" href="https://www.facebook.com/detiksportcom"  class="icon-item icon-item__fb">
							<i class="icon icon-facebook"></i>						</a>
						<a dtr-evt="footer" dtr-sec="sharefooter" dtr-act="share footer" onclick="_pt(this)" dtr-ttl="twitter"							target="_blank" href="https://twitter.com/detiksport"  class="icon-item icon-item__tw">
							<i class="icon icon-twitter"></i>						</a>
						<a dtr-evt="footer" dtr-sec="sharefooter" dtr-act="share footer" onclick="_pt(this)" dtr-ttl="instagram"							target="_blank" href="https://instagram.com/detiksport/"  class="icon-item icon-item__ig">
							<i class="icon icon-instagram"></i>						</a>
						<a dtr-evt="footer" dtr-sec="sharefooter" dtr-act="share footer" onclick="_pt(this)" dtr-ttl="linkedin"							target="_blank" href="https://www.linkedin.com/company/detik-com/"  class="icon-item icon-item__in">
							<i class="icon icon-linkedin"></i>						</a>
						<a dtr-evt="footer" dtr-sec="sharefooter" dtr-act="share footer" onclick="_pt(this)" dtr-ttl="youtube"							target="_blank" href="https://www.youtube.com/detikcom" class="icon-item icon-item__yt">
							<i class="icon icon-youtube"></i>						</a>
					</div>
					<div class="footer-title footer-title__copyright">Copyright @ 2022 detikcom. <br>All right reserved</div>
				</div>
				<nav class="column-3 footer-nav">
					<div class="footer-nav--title">Kategori</div>
					<ul class="nav nav--column nav--column-2">
                                                    <li class="nav__item">
								<a dtr-evt="footer" dtr-sec="menufooter" dtr-act="menu footer" onclick="_pt(this)" dtr-ttl="menu news"									href="https://news.detik.com" >
									News								</a>
                            </li>
                                                    <li class="nav__item">
								<a dtr-evt="footer" dtr-sec="menufooter" dtr-act="menu footer" onclick="_pt(this)" dtr-ttl="menu edukasi"									href="https://www.detik.com/edu" >
									Edukasi								</a>
                            </li>
                                                    <li class="nav__item">
								<a dtr-evt="footer" dtr-sec="menufooter" dtr-act="menu footer" onclick="_pt(this)" dtr-ttl="menu finance"									href="https://finance.detik.com" >
									Finance								</a>
                            </li>
                                                    <li class="nav__item">
								<a dtr-evt="footer" dtr-sec="menufooter" dtr-act="menu footer" onclick="_pt(this)" dtr-ttl="menu teknologi"									href="https://inet.detik.com" >
									Teknologi								</a>
                            </li>
                                                    <li class="nav__item">
								<a dtr-evt="footer" dtr-sec="menufooter" dtr-act="menu footer" onclick="_pt(this)" dtr-ttl="menu entertaiment"									href="https://hot.detik.com" >
									Entertaiment								</a>
                            </li>
                                                    <li class="nav__item">
								<a dtr-evt="footer" dtr-sec="menufooter" dtr-act="menu footer" onclick="_pt(this)" dtr-ttl="menu sport"									href="https://sport.detik.com" >
									Sport								</a>
                            </li>
                                                    <li class="nav__item">
								<a dtr-evt="footer" dtr-sec="menufooter" dtr-act="menu footer" onclick="_pt(this)" dtr-ttl="menu sepakbola"									href="https://sport.detik.com/sepakbola" >
									Sepakbola								</a>
                            </li>
                                                    <li class="nav__item">
								<a dtr-evt="footer" dtr-sec="menufooter" dtr-act="menu footer" onclick="_pt(this)" dtr-ttl="menu otomotif"									href="https://oto.detik.com" >
									Otomotif								</a>
                            </li>
                                                    <li class="nav__item">
								<a dtr-evt="footer" dtr-sec="menufooter" dtr-act="menu footer" onclick="_pt(this)" dtr-ttl="menu travel"									href="https://travel.detik.com" >
									Travel								</a>
                            </li>
                                                    <li class="nav__item">
								<a dtr-evt="footer" dtr-sec="menufooter" dtr-act="menu footer" onclick="_pt(this)" dtr-ttl="menu food"									href="https://food.detik.com" >
									Food								</a>
                            </li>
                                                    <li class="nav__item">
								<a dtr-evt="footer" dtr-sec="menufooter" dtr-act="menu footer" onclick="_pt(this)" dtr-ttl="menu health"									href="https://health.detik.com" >
									Health								</a>
                            </li>
                                                    <li class="nav__item">
								<a dtr-evt="footer" dtr-sec="menufooter" dtr-act="menu footer" onclick="_pt(this)" dtr-ttl="menu wolipop"									href="https://wolipop.detik.com" >
									Wolipop								</a>
                            </li>
                                                    <li class="nav__item">
								<a dtr-evt="footer" dtr-sec="menufooter" dtr-act="menu footer" onclick="_pt(this)" dtr-ttl="menu detikx"									href="https://news.detik.com/x/" >
									DetikX								</a>
                            </li>
                                                    <li class="nav__item">
								<a dtr-evt="footer" dtr-sec="menufooter" dtr-act="menu footer" onclick="_pt(this)" dtr-ttl="menu 20detik"									href="https://20.detik.com" >
									20Detik								</a>
                            </li>
                                                    <li class="nav__item">
								<a dtr-evt="footer" dtr-sec="menufooter" dtr-act="menu footer" onclick="_pt(this)" dtr-ttl="menu foto"									href="https://foto.detik.com" >
									Foto								</a>
                            </li>
                        					</ul>
				</nav>
				<nav class="column-2 footer-nav">
					<div class="footer-nav--title">Layanan</div>
					<ul class="nav nav--column">
                                                    <li class="nav__item">
								<a dtr-evt="footer" dtr-sec="menufooter" dtr-act="menu footer" onclick="_pt(this)" dtr-ttl="menu berbuatbaik.id"									href="https://berbuatbaik.id/" >
									berbuatbaik.id								</a>
                            </li>
                                                    <li class="nav__item">
								<a dtr-evt="footer" dtr-sec="menufooter" dtr-act="menu footer" onclick="_pt(this)" dtr-ttl="menu pasang mata"									href="https://pasangmata.detik.com" >
									Pasang Mata								</a>
                            </li>
                                                    <li class="nav__item">
								<a dtr-evt="footer" dtr-sec="menufooter" dtr-act="menu footer" onclick="_pt(this)" dtr-ttl="menu adsmart"									href="https://adsmart.detik.com" >
									Adsmart								</a>
                            </li>
                                                    <li class="nav__item">
								<a dtr-evt="footer" dtr-sec="menufooter" dtr-act="menu footer" onclick="_pt(this)" dtr-ttl="menu forum"									href="https://forum.detik.com" >
									Forum								</a>
                            </li>
                                                    <li class="nav__item">
								<a dtr-evt="footer" dtr-sec="menufooter" dtr-act="menu footer" onclick="_pt(this)" dtr-ttl="menu detikevent"									href="https://event.detik.com" >
									detikEvent								</a>
                            </li>
                                                    <li class="nav__item">
								<a dtr-evt="footer" dtr-sec="menufooter" dtr-act="menu footer" onclick="_pt(this)" dtr-ttl="menu trans snow world"									href="https://www.transsnowworld.com/" >
									Trans Snow World								</a>
                            </li>
                                                    <li class="nav__item">
								<a dtr-evt="footer" dtr-sec="menufooter" dtr-act="menu footer" onclick="_pt(this)" dtr-ttl="menu trans studio"									href="https://www.transstudiobandung.com/" >
									Trans Studio								</a>
                            </li>
                        					</ul>
				</nav>
				<nav class="column-2 footer-nav">
					<div class="footer-nav--title">Informasi</div>
					<ul class="nav nav--column">
                                                    <li class="nav__item">
								<a dtr-evt="footer" dtr-sec="menufooter" dtr-act="menu footer" onclick="_pt(this)" dtr-ttl="menu redaksi"									href="https://www.detik.com/redaksi" >
									Redaksi								</a>
                            </li>
                                                    <li class="nav__item">
								<a dtr-evt="footer" dtr-sec="menufooter" dtr-act="menu footer" onclick="_pt(this)" dtr-ttl="menu pedoman media siber"									href="https://www.detik.com/pedoman-media" rel="nofollow">
									Pedoman Media Siber								</a>
                            </li>
                                                    <li class="nav__item">
								<a dtr-evt="footer" dtr-sec="menufooter" dtr-act="menu footer" onclick="_pt(this)" dtr-ttl="menu karir"									href="https://www.detik.com/karir" rel="nofollow">
									Karir								</a>
                            </li>
                                                    <li class="nav__item">
								<a dtr-evt="footer" dtr-sec="menufooter" dtr-act="menu footer" onclick="_pt(this)" dtr-ttl="menu kotak pos"									href="https://www.detik.com/kotak-pos" rel="nofollow">
									Kotak Pos								</a>
                            </li>
                                                    <li class="nav__item">
								<a dtr-evt="footer" dtr-sec="menufooter" dtr-act="menu footer" onclick="_pt(this)" dtr-ttl="menu media partner"									href="https://www.detik.com/media-partner" rel="nofollow">
									Media Partner								</a>
                            </li>
                                                    <li class="nav__item">
								<a dtr-evt="footer" dtr-sec="menufooter" dtr-act="menu footer" onclick="_pt(this)" dtr-ttl="menu info iklan"									href="https://www.detik.com/info-iklan" rel="nofollow">
									Info Iklan								</a>
                            </li>
                                                    <li class="nav__item">
								<a dtr-evt="footer" dtr-sec="menufooter" dtr-act="menu footer" onclick="_pt(this)" dtr-ttl="menu privacy policy"									href="https://www.detik.com/privacy-policy" rel="nofollow">
									Privacy Policy								</a>
                            </li>
                                                    <li class="nav__item">
								<a dtr-evt="footer" dtr-sec="menufooter" dtr-act="menu footer" onclick="_pt(this)" dtr-ttl="menu disclaimer"									href="https://www.detik.com/disclaimer" rel="nofollow">
									Disclaimer								</a>
                            </li>
                        					</ul>
				</nav>
				<nav class="column-2 footer-nav">
					<div class="footer-nav--title">Jaringan Media</div>
					<ul class="nav nav--column">
                                                    <li class="nav__item">
								<a dtr-evt="footer" dtr-sec="menufooter" dtr-act="menu footer" onclick="_pt(this)" dtr-ttl="menu cnn indonesia"									href="https://www.cnnindonesia.com"
									target="_blank" >
									CNN Indonesia								</a>
                            </li>
                                                    <li class="nav__item">
								<a dtr-evt="footer" dtr-sec="menufooter" dtr-act="menu footer" onclick="_pt(this)" dtr-ttl="menu cnbc indonesia"									href="https://www.cnbcindonesia.com"
									target="_blank" >
									CNBC Indonesia								</a>
                            </li>
                                                    <li class="nav__item">
								<a dtr-evt="footer" dtr-sec="menufooter" dtr-act="menu footer" onclick="_pt(this)" dtr-ttl="menu haibunda"									href="https://www.haibunda.com"
									target="_blank" >
									Haibunda								</a>
                            </li>
                                                    <li class="nav__item">
								<a dtr-evt="footer" dtr-sec="menufooter" dtr-act="menu footer" onclick="_pt(this)" dtr-ttl="menu insertlive"									href="https://www.insertlive.com/"
									target="_blank" >
									Insertlive								</a>
                            </li>
                                                    <li class="nav__item">
								<a dtr-evt="footer" dtr-sec="menufooter" dtr-act="menu footer" onclick="_pt(this)" dtr-ttl="menu beautynesia"									href="https://beautynesia.id"
									target="_blank" >
									Beautynesia								</a>
                            </li>
                                                    <li class="nav__item">
								<a dtr-evt="footer" dtr-sec="menufooter" dtr-act="menu footer" onclick="_pt(this)" dtr-ttl="menu female daily"									href="https://femaledaily.com"
									target="_blank" >
									Female Daily								</a>
                            </li>
                                                    <li class="nav__item">
								<a dtr-evt="footer" dtr-sec="menufooter" dtr-act="menu footer" onclick="_pt(this)" dtr-ttl="menu cxo media"									href="https://www.cxomedia.id"
									target="_blank" >
									CXO Media								</a>
                            </li>
                        					</ul>
				</nav>
			</div>
		</div>
	</div>
	<div d-widget="bytedance" d-loaded="1" style="display:none"></div>
</footer>

<!-- S:balloonads --><div id="timesignal2022"></div>
<!-- /4905536/detik_desktop/sepakbola/balloon_ads -->
<div id='div-gpt-ad-1587568268835-0' style='width: 1px; height: 1px;'>
<script>
googletag.cmd.push(function() { googletag.display('div-gpt-ad-1587568268835-0'); });
</script>
</div><!-- E:balloonads -->
<!-- S:bottomframe --><style>#stickyunit{z-index:999;}</style>
<div class="bottom_banner_bar" id="bottom_banner" style="display:none;background-color: #ffffff;min-height:0px">
<div id="adv-callback" style="position:absolute;text-align: center;left: 0;right: 0;line-height: 7;border:1px solid;width: 728px;height: 90px;display:none;margin:auto;">Advertisement</div>
<div class="bottom_banner" style="width:728px;">
<div id="bottomframe"></div>
<!-- /4905536/detik_desktop/sepakbola/bottomframe -->
<div id='div-gpt-ad-1605763052301-0'>
  <script>
    googletag.cmd.push(function() { googletag.display('div-gpt-ad-1605763052301-0'); });
  </script>
</div>
<span class="close_tbot" style="margin-right: 0px;display:none;"><div style="color: white;font-size: small;font-weight: bold;background-color: black;text-align: center;line-height: 1;padding: 03px 10px;margin: -5px;">CLOSE</div></span>

<script>
$(document).ready(function(){
$('.close_tbot').click(function(){
$('.bottom_banner').remove();
$('#bottom_banner').remove();
})
})
</script>

</div>
</div><!-- E:bottomframe -->
<script type="text/javascript" src="https://cdn.detik.net.id/assets/js/oembed.itp.v2.js?v=2022040815"></script>

<!--widget:popout_notif-->

<!-- S:tagfoot --><script type="text/javascript" src="https://cdn.detik.net.id/commerce/desktop/js/detik.ads.controller.js"></script>

<script>
var loadScriptAsync = function(uri){
  return new Promise((resolve, reject) => {
    var tag = document.createElement('script');
    tag.src = uri;
    tag.async = true;
    tag.onload = () => {
      resolve();
    };
  var firstScriptTag = document.getElementsByTagName('script')[0];
  firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
});
}
var scriptLoaded = loadScriptAsync('https://newrevive.detik.com/delivery/asyncjs.php');

scriptLoaded.then(function(){
  window.reviveAsync["0cceecb9cae9f51a31123c541910d59b"].addEventListener('afterRenderBanner',rvCallbackBanner);
 
  function rvCallbackBanner(data){
 
  for(i=0;i<data.detail.length;i++){
    checkBannerAvailable(data.detail[i], 192,detectHeightMR2,true);
    checkBannerAvailable(data.detail[i], 190, nullLB);
    checkBannerAvailable(data.detail[i], 718, nullTB);
    checkBannerAvailable(data.detail[i], 719, nullBB);
    checkBannerAvailable(data.detail[i], 723, recalc, true);
    checkBannerAvailable(data.detail[i], 2556, recalc, true);
    checkBannerAvailable(data.detail[i], 195, recalc, true);
   }
 
  }
});

function checkBannerAvailable(dataDetail, checkZone, callFunc, available = false){
	try {
		var show = available ? 1 : 0;
		if(show == 0){
          if (dataDetail.zoneid == checkZone && dataDetail.bannerid == show) {
            callFunc();
            console.log('zone id adjustment : ' + checkZone);
          }
        }
        if(show == 1){
          if (dataDetail.zoneid == checkZone && dataDetail.bannerid >= show) {
            callFunc();
            console.log('zone id adjustment : ' + checkZone);
          }
        }
	} catch(e) {
		console.log(e);
	}
}

</script>



<!-- Pixel Tag Audience DFP -->  

<script type='text/javascript'>
var axel = Math.random() + '';
var a = axel * 10000000000000;
document.write('<img src="https://pubads.g.doubleclick.net/activity;dc_iu=/4905536/DFPAudiencePixel;ord=' + a + ';dc_seg=1010791389;gen=Male?" width=1 height=1 border=0/>');
</script>
<noscript>
<img src="https://pubads.g.doubleclick.net/activity;dc_iu=/4905536/DFPAudiencePixel;ord=1;dc_seg=1010791389;gen=Male?" width=1 height=1 border=0/>
</noscript>

<script type='text/javascript'>
var axel = Math.random() + '';
var a = axel * 10000000000000;
document.write('<img src="https://pubads.g.doubleclick.net/activity;dc_iu=/4905536/DFPAudiencePixel;ord=' + a + ';dc_seg=1009875724;gen=Female?" width=1 height=1 border=0/>');
</script>
<noscript>
<img src="https://pubads.g.doubleclick.net/activity;dc_iu=/4905536/DFPAudiencePixel;ord=1;dc_seg=1009875724;gen=Female?" width=1 height=1 border=0/>
</noscript>

<!-- End Pixel Tag Audience DFP -->  
<!-- E:tagfoot -->
<!--s:dtkprv-->
<!-- Begin comScore Tag -->
<script>
  var _comscore = _comscore || [];
  _comscore.push({ c1: "2", c2: "8443234" });
  (function() {
    var s = document.createElement("script"), el = document.getElementsByTagName("script")[0]; s.async = true;
    s.src = (document.location.protocol == "https:" ? "https://sb" : "https://b") + ".scorecardresearch.com/beacon.js";
    el.parentNode.insertBefore(s, el);
  })();
</script>
<noscript>
  <img src="https://b.scorecardresearch.com/p?c1=2&c2=8443234&cv=2.0&cj=1" />
</noscript>
<!-- End comScore Tag -->
<!--e:dtkprv-->    </body>
</html>
<!--replaced--><!--0-->