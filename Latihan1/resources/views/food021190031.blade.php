<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>PIZZA</title>
</head>
<body>
    <h1>PIZZA</h1>
    <p>Piza (menurut KBBI) (atau pizza) adalah hidangan gurih dari Italia sejenis adonan bundar dan pipih, yang dipanggang di oven dan biasanya dilumuri saus tomat serta keju dengan bahan makanan tambahan lainnya yang bisa dipilih. Keju yang dipakai biasanya mozzarella atau "keju pizza", bisa juga keju parmesan dan beberapa keju lainnya.

Jenis bahan lain juga dapat ditaruh di atas pizza, biasanya daging dan saus, seperti salami dan pepperoni, ham, bacon, buah seperti nanas dan zaitun, sayuran seperti cabe dan paprika, dan juga bawang bombay, jamur dan lain lain.

Rotinya biasa dibuat seperti roti biasa namun bisa diberi rasa tambahan dengan mentega, bawang putih, tanaman herbal, atau wijen. Pizza biasanya dibuat dengan memutar-mutarkan adonan roti yang menjadi pipih. Hal tersebut juga sering dijadikan atraksi bagi beberapa toko Pizzeria. Pizza biasanya dimakan selagi panas (biasanya untuk makan siang dan malam), tetapi ada pula yang disajikan dingin, biasanya dimakan untuk sarapan atau saat piknik.

Pizza biasanya dimakan di restoran, dibeli di pasar grosir atau supermarket, dan dapat pula dipesan melalui telepon atau ini melalui web untuk diantar, panas dan siap untuk disantap di rumah.

Kata "pizza" diambil dari bahasa Italia pizza (Alfabet Fonetik Internasional / International Phonetic Alphabet, IPA: [ˈpiːtsə]), biasanya berarti "pai, kue, tart". Banyak yang salah mengira bahwa pizza berasal dari kata Italia yang berarti phai (pie).</p>
</body>
</html>